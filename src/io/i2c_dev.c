#include "i2c_dev.h"
#include "hardware/sync.h"
#include <hardware/i2c.h>
#include <string.h>

#define MASK(amount) ((1 << (amount)) - 1)

#define ERR_CHECK(dev, i2c_call)                                 \
    do {                                                         \
        if ((dev)->error == 0) {                                 \
            uint32_t interrupts = save_and_disable_interrupts(); \
            int ret = (i2c_call);                                \
            restore_interrupts(interrupts);                      \
            if (ret < 0) {                                       \
                (dev)->error = ret;                              \
            }                                                    \
        }                                                        \
    } while (0)

bool i2c_dev_read_bit(struct i2c_dev* dev, struct i2c_dev_bit bit)
{
    return i2c_dev_read_bits(dev, I2C_DEV_BITS(bit.reg, bit.offset, 1));
}

uint8_t i2c_dev_read_bits(struct i2c_dev* dev, struct i2c_dev_bits bits)
{
    uint8_t byte = i2c_dev_read_byte(dev, bits.reg);
    return (byte >> bits.offset) & MASK(bits.length);
}

uint8_t i2c_dev_read_byte(struct i2c_dev* dev, uint8_t reg)
{
    uint8_t read_byte = 0;
    i2c_dev_read(dev, reg, &read_byte, 1);
    return read_byte;
}

uint16_t i2c_dev_read_short(struct i2c_dev* dev, uint8_t reg)
{
    uint16_t read_short = 0;
    i2c_dev_read(dev, reg, &read_short, 2);
    return read_short;
}

void i2c_dev_read(struct i2c_dev* dev, uint8_t reg, void* dst, size_t size)
{
    ERR_CHECK(dev, i2c_write_blocking(dev->i2c, dev->addr, &reg, 1, false));
    ERR_CHECK(dev, i2c_read_blocking(dev->i2c, dev->addr, dst, size, false));
}

void i2c_dev_write_bit(struct i2c_dev* dev, struct i2c_dev_bit bit, bool value)
{
    i2c_dev_write_bits(dev, I2C_DEV_BITS(bit.reg, bit.offset, 1), value);
}

void i2c_dev_write_bits(struct i2c_dev* dev, struct i2c_dev_bits bits, uint8_t value)
{
    uint8_t prev_byte = i2c_dev_read_byte(dev, bits.reg);

    uint8_t offset_mask = MASK(bits.length) << bits.offset;
    uint8_t byte = (prev_byte & ~offset_mask) | ((value << bits.offset) & offset_mask);

    i2c_dev_write_byte(dev, bits.reg, byte);
}

void i2c_dev_write_byte(struct i2c_dev* dev, uint8_t reg, uint8_t value)
{
    uint8_t payload[] = { reg, value };
    ERR_CHECK(dev, i2c_write_blocking(dev->i2c, dev->addr, payload, sizeof(payload), false));
}

void i2c_dev_write(struct i2c_dev* dev, uint8_t reg, const void* src, size_t size)
{
    uint8_t payload[32];
    assert(size + 1 < sizeof(payload));

    payload[0] = reg;
    memcpy(&payload[1], src, size);

    ERR_CHECK(dev, i2c_write_blocking(dev->i2c, dev->addr, payload, size + 1, false));
}
