#pragma once
#include <hardware/i2c.h>

struct i2c_dev {
    i2c_inst_t* i2c;
    uint8_t addr;
    int error;
};

struct i2c_dev_bit {
    uint8_t reg;
    uint8_t offset;
};

struct i2c_dev_bits {
    uint8_t reg;
    uint8_t offset;
    uint8_t length;
};

#define I2C_DEV_BITS(reg, offset, length) \
    (struct i2c_dev_bits) { (reg), (offset), (length) }
#define I2C_DEV_BIT(reg, offset) \
    (struct i2c_dev_bit) { (reg), (offset) }

bool i2c_dev_read_bit(struct i2c_dev* dev, struct i2c_dev_bit bit);
uint8_t i2c_dev_read_bits(struct i2c_dev* dev, struct i2c_dev_bits bits);
uint8_t i2c_dev_read_byte(struct i2c_dev* dev, uint8_t reg);
uint16_t i2c_dev_read_short(struct i2c_dev* dev, uint8_t reg);
void i2c_dev_read(struct i2c_dev* dev, uint8_t reg, void* dst, size_t size);

void i2c_dev_write_bit(struct i2c_dev* dev, struct i2c_dev_bit bit, bool value);
void i2c_dev_write_bits(struct i2c_dev* dev, struct i2c_dev_bits bits, uint8_t value);
void i2c_dev_write_byte(struct i2c_dev* dev, uint8_t reg, uint8_t value);
void i2c_dev_write(struct i2c_dev* dev, uint8_t reg, const void* src, size_t size);
