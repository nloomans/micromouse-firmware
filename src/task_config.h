#pragma once
#include <task.h>

enum task_priority {
    TASK_PRIO_SHELL = 2,
    // Shell processes are given a lower priority than the shell itself, to
    // make sure it can't prevent the Shell from stopping the task.
    TASK_PRIO_SHELL_PROCESS = 1,
    // Priority is equal to the IDLE priority. This is because LegacyMain
    // will use all CPU resources if it gets given the chance, meaning that
    // if we give it a higher priority than IDLE, IDLE will never run.
    TASK_PRIO_LEGACY_MAIN = 0,
};
