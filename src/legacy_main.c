#include "control/control.h"
#include "drivers/button.h"
#include "drivers/gyro.h"
#include "drivers/ir.h"
#include "drivers/motor.h"
#include "drivers/pins.h"
#include "drivers/shift.h"
#include "interrupt.h"
#include "lib/ipc.h"
#include "log.h"
#include "multicore_test.h"
#include "shell_compat.h"
#include "task_config.h"
#include "test/solve_maze.h"
#include <pico/stdlib.h>
#include <stdbool.h>
#include <task.h>

static void task(void* data)
{
    log_init();

    interrupt_init();
    shift_init();

    // Enable an LED so we know the device is on
    shift_put(SPIN_LED_1, 1);
    shift_flush();

    ir_init();
    gyro_init();
    button_init();
    motor_init();

    ipc_init();

    control_init();
    multicore_test_init();

    solve_maze_test_init();

    uint64_t iteration = 0;
    while (true) {
        if (iteration % 4000 == 0) {
            shift_put(SPIN_LED_1, 1);
            shift_flush();
        } else if (iteration % 4000 == 2000) {
            shift_put(SPIN_LED_1, 0);
            shift_flush();
        }

        shell_compat_tick();

        tight_loop_contents();

        iteration++;
    }
}

void legacy_main_init(void)
{
    // We need a large stack size since solve_maze_test_init() may use a bunch
    // of stack while generating a random maze using fill_randomly();
    // TODO: Fix this more elegently. We probably don't want to create this
    // test maze during init at all.
    static StackType_t legacy_main_stack[configMINIMAL_STACK_SIZE + 2048];
    static StaticTask_t legacy_main_store;

    xTaskCreateStatic(&task, "LegacyMain",
        ARRAY_SIZE(legacy_main_stack),
        NULL,
        TASK_PRIO_LEGACY_MAIN,
        legacy_main_stack,
        &legacy_main_store);
}
