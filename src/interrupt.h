#pragma once
#include "hardware/gpio.h"
#include "lib/macro.h"

struct interrupt_gpio_registration {
    unsigned int gpio;
    uint32_t event_mask;
    gpio_irq_callback_t handler;

    struct interrupt_gpio_registration* next;
};

void interrupt_init();

void _interrupt_gpio_register(struct interrupt_gpio_registration* registration);

#define interrupt_gpio_register(irq_gpio, irq_event_mask, irq_handler)          \
    static struct interrupt_gpio_registration MACRO_LOCAL(irq_registration) = { \
        .gpio = (irq_gpio),                                                     \
        .event_mask = (irq_event_mask),                                         \
        .handler = (irq_handler),                                               \
    };                                                                          \
                                                                                \
    _interrupt_gpio_register(&MACRO_LOCAL(irq_registration))
