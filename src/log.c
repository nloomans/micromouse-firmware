#include "pico/mutex.h"
#include "pico/time.h"
#include "shell_compat.h"
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

static mutex_t mutex;
static char buffer[20 * 1024] = { 0 };
static size_t end = 0;

void log_vprintf(const char* fmt, va_list args)
{
#if 0
    mutex_enter_blocking(&mutex);
    // vprintf(fmt, args);
    int space_left = sizeof(buffer) - 1 - end;

    if (space_left > 0) {
        end += vsnprintf(buffer + end, space_left, fmt, args);
    }
	if (end >= sizeof(buffer) - 1) {
		end = 0;
	}

    mutex_exit(&mutex);
#endif
}

void log_printf(const char* fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    log_vprintf(fmt, args);
    va_end(args);
}

void log_stampf(const char* tag, const char* fmt, ...)
{
    log_printf("[%-7s %12llu] ", tag, get_absolute_time());

    va_list args;
    va_start(args, fmt);
    log_vprintf(fmt, args);
    va_end(args);

    log_printf("\n");
}

void log_clear()
{
    mutex_enter_blocking(&mutex);
    memset(buffer, 0, sizeof(buffer));
    end = 0;
    mutex_exit(&mutex);
}

void cmd_log(shell_compat_process_t* process, int argc, char** argv)
{
    printf("%s", buffer + end + 2);
    printf("%s", buffer);
    log_clear();
    shell_compat_exit(process);
}

void log_init()
{
    mutex_init(&mutex);
    shell_compat_register("log", "Print messages that were printed using log_printf", cmd_log);
}
