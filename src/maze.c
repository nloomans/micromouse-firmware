#include "maze.h"
#include "lib/macro.h"
#include "log.h"
#include "pico/assert.h"
#include "pico/time.h"
#include "units.h"
#include <assert.h>
#include <stdio.h>
#include <string.h>

enum field_kind {
    // There is no maze here.
    FIELD_KIND_OUT_OF_BOUNDS,

    // Unused corner fields, you can pretend it represents the lattice points.
    FIELD_KIND_LATTICE_POINT,

    // The robot is always located on one of these
    FIELD_KIND_SQUARE,

    // This field *may* contain a wall.
    FIELD_KIND_WALL,
};

static enum field_kind get_field_kind(struct vector pos)
{
    if (pos.y < 0 || pos.y >= MAZE_FIELDS_WIDTH
        || pos.x < 0 || pos.x >= MAZE_FIELDS_WIDTH) {
        return FIELD_KIND_OUT_OF_BOUNDS;
    } else if (pos.y % 2 == 0 && pos.x % 2 == 0) {
        return FIELD_KIND_LATTICE_POINT;
    } else if (pos.y % 2 == 1 && pos.x % 2 == 1) {
        return FIELD_KIND_SQUARE;
    } else {
        return FIELD_KIND_WALL;
    }
}

void maze_init(struct maze* maze)
{
    memset(maze, 0, sizeof(*maze));

    // We know where the outer walls are, so pre-set those.
    for (int x = 1; x < MAZE_FIELDS_WIDTH; x += 2) {
        maze->fields[0][x] = MAZE_FIELD_WALL;
    }

    for (int y = 1; y < MAZE_FIELDS_WIDTH; y += 2) {
        maze->fields[y][0] = MAZE_FIELD_WALL;
        maze->fields[y][MAZE_FIELDS_WIDTH - 1] = MAZE_FIELD_WALL;
    }

    for (int x = 1; x < MAZE_FIELDS_WIDTH; x += 2) {
        maze->fields[MAZE_FIELDS_WIDTH - 1][x] = MAZE_FIELD_WALL;
    }
}

enum maze_field maze_get_field(const struct maze* maze, struct vector vector)
{
    if (!(vector.y >= 0 && vector.y < MAZE_FIELDS_WIDTH && vector.x >= 0 && vector.x < MAZE_FIELDS_WIDTH)) {
        printf("tried to read y %d x %d\n", vector.y, vector.x);
        sleep_ms(1);
    }
    hard_assert(vector.y >= 0 && vector.y < MAZE_FIELDS_WIDTH
        && vector.x >= 0 && vector.x < MAZE_FIELDS_WIDTH);
    return maze->fields[vector.y][vector.x];
}

void maze_set_field(struct maze* maze, struct vector vector, enum maze_field value)
{
    if (!(vector.y >= 0 && vector.y < MAZE_FIELDS_WIDTH && vector.x >= 0 && vector.x < MAZE_FIELDS_WIDTH)) {
        printf("tried to write %d to y %d x %d\n", value, vector.y, vector.x);
        sleep_ms(1);
    }
    hard_assert(vector.y >= 0 && vector.y < MAZE_FIELDS_WIDTH
        && vector.x >= 0 && vector.x < MAZE_FIELDS_WIDTH);
    maze->fields[vector.y][vector.x] = value;
}

void maze_set_square(struct maze* maze, struct vector pos, enum maze_field value)
{
    assert(pos.y % 2 == 1 && pos.x % 2 == 1);
    assert(value == MAZE_FIELD_UNKNOWN || value == MAZE_FIELD_EMPTY);
    maze_set_field(maze, pos, value);
}

enum maze_field maze_get_wall(const struct maze* maze,
    struct vector pos, enum car_dir car_dir)
{
    assert(pos.y % 2 == 1 && pos.x % 2 == 1);

    return maze_get_field(maze,
        vector_add(pos, car_dir_to_vector(car_dir)));
}

void maze_set_wall(struct maze* maze,
    struct vector pos, enum car_dir car_dir, enum maze_field value)
{
    assert(pos.y % 2 == 1 && pos.x % 2 == 1);

    maze_set_field(maze,
        vector_add(pos, car_dir_to_vector(car_dir)), value);
}

struct vector maze_square_get_neighbour_pos(const struct vector square_pos,
    enum car_dir car_dir)
{
    assert(square_pos.y % 2 == 1 && square_pos.x % 2 == 1);

    struct vector offset = car_dir_to_vector(car_dir);
    offset.y *= 2;
    offset.x *= 2;
    return vector_add(square_pos, offset);
}

bool maze_is_within_bounds(struct vector pos)
{
    return get_field_kind(pos) != FIELD_KIND_OUT_OF_BOUNDS;
}

bool maze_is_starting_square_known(const struct maze* maze)
{
    return maze->starting_square.y != 0 || maze->starting_square.x != 0;
}

void maze_set_starting_square(struct maze* maze, struct vector starting_square)
{
    assert(!maze_is_starting_square_known(maze));

    maze->starting_square = starting_square;

    // The rules dictate:
    //
    //  "4.4. Start/End Zones
    //   The starting square of the maze is located at one of the four corners.
    //   The starting square is bounded on three sides by walls."
    //
    // Hence we may prefill the maze as so:
    maze_set_square(maze, starting_square, MAZE_FIELD_EMPTY);
    maze_set_wall(maze, starting_square, CAR_DIR_NORTH, MAZE_FIELD_EMPTY);
    maze_set_wall(maze, starting_square, CAR_DIR_EAST, MAZE_FIELD_WALL);
    maze_set_wall(maze, starting_square, CAR_DIR_SOUTH, MAZE_FIELD_WALL);
    maze_set_wall(maze, starting_square, CAR_DIR_WEST, MAZE_FIELD_WALL);
}

static char* square_string(const struct maze* maze, struct vector pos)
{
    assert(get_field_kind(pos) == FIELD_KIND_SQUARE);

    switch (maze_get_field(maze, pos)) {
    case MAZE_FIELD_UNKNOWN:
        return "░░░";
    case MAZE_FIELD_EMPTY:
        return " · ";
    default:
        return " # ";
    }
}

static char* wall_string(const struct maze* maze, struct vector pos)
{
    assert(get_field_kind(pos) == FIELD_KIND_WALL);

    if (pos.y % 2 == 0) {
        // Horizontal wall
        switch (maze_get_field(maze, pos)) {
        case MAZE_FIELD_UNKNOWN:
            return "░░░";
        case MAZE_FIELD_EMPTY:
            return "   ";
        case MAZE_FIELD_WALL:
            return "───";
        default:
            return " # ";
        }
    } else {
        switch (maze_get_field(maze, pos)) {
        case MAZE_FIELD_UNKNOWN:
            return "░";
        case MAZE_FIELD_EMPTY:
            return " ";
        case MAZE_FIELD_WALL:
            return "│";
        default:
            return "#";
        }
    }
}

static char* lattice_point_string(const struct maze* maze, struct vector pos)
{
    assert(get_field_kind(pos) == FIELD_KIND_LATTICE_POINT);

    enum maze_field neighbours[] = {
        [CAR_DIR_NORTH] = pos.y > 0
            ? maze->fields[pos.y - 1][pos.x]
            : MAZE_FIELD_UNKNOWN,

        [CAR_DIR_EAST] = pos.x < MAZE_FIELDS_WIDTH - 1
            ? maze->fields[pos.y][pos.x + 1]
            : MAZE_FIELD_UNKNOWN,

        [CAR_DIR_SOUTH] = pos.y < MAZE_FIELDS_WIDTH - 1
            ? maze->fields[pos.y + 1][pos.x]
            : MAZE_FIELD_UNKNOWN,

        [CAR_DIR_WEST] = pos.x > 0
            ? maze->fields[pos.y][pos.x - 1]
            : MAZE_FIELD_UNKNOWN,
    };

    bool all_unknown
        = neighbours[CAR_DIR_NORTH] == MAZE_FIELD_UNKNOWN
        && neighbours[CAR_DIR_EAST] == MAZE_FIELD_UNKNOWN
        && neighbours[CAR_DIR_SOUTH] == MAZE_FIELD_UNKNOWN
        && neighbours[CAR_DIR_WEST] == MAZE_FIELD_UNKNOWN;

    if (all_unknown) {
        return "░";
    }

    bool north = neighbours[CAR_DIR_NORTH] == MAZE_FIELD_WALL;
    bool east = neighbours[CAR_DIR_EAST] == MAZE_FIELD_WALL;
    bool south = neighbours[CAR_DIR_SOUTH] == MAZE_FIELD_WALL;
    bool west = neighbours[CAR_DIR_WEST] == MAZE_FIELD_WALL;

    if (north && east && south && west)
        return "┼";

    if (east && south && west)
        return "┬";
    if (north && south && west)
        return "┤";
    if (north && east && west)
        return "┴";
    if (north && east && south)
        return "├";

    if (north && east)
        return "└";
    if (north && south)
        return "│";
    if (north && west)
        return "┘";
    if (east && south)
        return "┌";
    if (east && west)
        return "─";
    if (south && west)
        return "┐";

    if (north)
        return "╵";
    if (east)
        return "╶";
    if (south)
        return "╷";
    if (west)
        return "╴";

    return " ";
}

static char* field_string(const struct maze* maze, struct vector pos)
{
    switch (get_field_kind(pos)) {
    case FIELD_KIND_OUT_OF_BOUNDS:
        assert(0);
        return NULL;
    case FIELD_KIND_LATTICE_POINT:
        return lattice_point_string(maze, pos);
    case FIELD_KIND_SQUARE:
        return square_string(maze, pos);
    case FIELD_KIND_WALL:
        return wall_string(maze, pos);
    }

    macro_unreachable();
}

void maze_print(const struct maze* maze)
{
    for (int y = 0; y < MAZE_FIELDS_WIDTH; y++) {
        for (int x = 0; x < MAZE_FIELDS_WIDTH; x++) {
            log_printf("%s", field_string(maze, (struct vector) { y, x }));
        }
        log_printf("\n");
    }
}

void maze_print_with_robot(const struct maze* maze, struct vector pos, enum car_dir car_dir)
{
    for (int y = 0; y < MAZE_FIELDS_WIDTH; y++) {
        for (int x = 0; x < MAZE_FIELDS_WIDTH; x++) {
            if (pos.y == y && pos.x == x) {
                char* arrow
                    = car_dir == CAR_DIR_NORTH ? "🢁"
                    : car_dir == CAR_DIR_EAST  ? "🢂"
                    : car_dir == CAR_DIR_SOUTH ? "🢃"
                    : car_dir == CAR_DIR_WEST  ? "🢀"
                                               : "?";

                log_printf(" %s ", arrow);
            } else {
                log_printf("%s", field_string(maze, (struct vector) { y, x }));
            }
        }
        log_printf("\n");
    }
}
