#pragma once
#include "config.h"
#include "units.h"
#include <stdbool.h>

#define MAZE_SQUARES CONFIG_MAZE_SIZE
#define MAZE_FIELDS_WIDTH (MAZE_SQUARES * 2 + 1)

enum maze_field {
    MAZE_FIELD_UNKNOWN,
    MAZE_FIELD_EMPTY,
    MAZE_FIELD_WALL,
} __attribute__((__packed__));

struct maze {
    /*
     * 2d array for storing the maze data. The structure is as follows:
     *
     *     0 1 2 3 4 5 6 7 8 9 A B C D E F
     *    .W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.   Legend
     *  0 WSWSWSWSWSWSWSWSWSWSWSWSWSWSWSWSW   ======
     *    .W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.
     *  1 WSWSWSWSWSWSWSWSWSWSWSWSWSWSWSWSW   S = Square
     *    .W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.   W = possible Wall location
     *  2 WSWSWSWSWSWSWSWSWSWSWSWSWSWSWSWSW   . = unused, always zero.
     *    .W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.
     *  3 WSWSWSWSWSWSWSWSWSWSWSWSWSWSWSWSW   Square
     *    .W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.   ======
     *  4 WSWSWSWSWSWSWSWSWSWSWSWSWSWSWSWSW
     *    .W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.   MAZE_FIELD_UNKNOWN = not visited
     *  5 WSWSWSWSWSWSWSWSWSWSWSWSWSWSWSWSW   MAZE_FIELD_EMPTY = visited
     *    .W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.
     *  6 WSWSWSWSWSWSWSWSWSWSWSWSWSWSWSWSW   Wall
     *    .W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.   ====
     *  8 WSWSWSWSWSWSWSWSWSWSWSWSWSWSWSWSW
     *    .W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.   MAZE_FIELD_UNKNOWN = unknown
     *  9 WSWSWSWSWSWSWSWSWSWSWSWSWSWSWSWSW   MAZE_FIELD_EMPTY = path is open
     *    .W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.   MAZE_FIELD_WALL = path is blocked
     *  A WSWSWSWSWSWSWSWSWSWSWSWSWSWSWSWSW
     *    .W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.
     *  B WSWSWSWSWSWSWSWSWSWSWSWSWSWSWSWSW
     *    .W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.
     *  C WSWSWSWSWSWSWSWSWSWSWSWSWSWSWSWSW
     *    .W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.
     *  D WSWSWSWSWSWSWSWSWSWSWSWSWSWSWSWSW
     *    .W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.
     *  E WSWSWSWSWSWSWSWSWSWSWSWSWSWSWSWSW
     *    .W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.
     *  F WSWSWSWSWSWSWSWSWSWSWSWSWSWSWSWSW
     *    .W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.W.
     *
     * The other walls will always be set to MAZW_FIELD_WALL.
     */
    enum maze_field fields[MAZE_FIELDS_WIDTH][MAZE_FIELDS_WIDTH];
    struct vector starting_square;
};

#define MAZE_STARTING_SQUARE_BOTTOM_LEFT \
    ((struct vector) { MAZE_FIELDS_WIDTH - 2, 1 })
#define MAZE_STARTING_SQUARE_BOTTOM_RIGHT \
    ((struct vector) { MAZE_FIELDS_WIDTH - 2, MAZE_FIELDS_WIDTH - 2 })

void maze_init(struct maze* maze);

enum maze_field maze_get_field(const struct maze* maze, struct vector pos);
void maze_set_field(struct maze* maze, struct vector pos, enum maze_field value);

void maze_set_square(struct maze* maze, struct vector pos, enum maze_field value);

enum maze_field maze_get_wall(const struct maze* maze,
    struct vector pos, enum car_dir dir_from_pos);

void maze_set_wall(struct maze* maze,
    struct vector pos, enum car_dir car_dir, enum maze_field value);

struct vector maze_square_get_neighbour_pos(const struct vector square_pos,
    enum car_dir car_dir);

bool maze_is_within_bounds(struct vector pos);

bool maze_is_starting_square_known(const struct maze* maze);
void maze_set_starting_square(struct maze* maze, struct vector starting_square);

void maze_print(const struct maze* maze);

void maze_print_with_robot(const struct maze* maze, struct vector pos, enum car_dir car_dir);
