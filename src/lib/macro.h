#pragma once

// Hack so we can construct variables with other macros
// https://stackoverflow.com/a/1489985
#define __MACRO_CONACT2(a, b) a##b
#define __MACRO_CONCAT1(a, b) __MACRO_CONACT2(a, b)
#define MACRO_CONCAT(a, b) __MACRO_CONCAT1(a, b)

/**
 * MACRO_LOCAL constructs an identifier that can be reasonably assumed to be
 * unique within the macro. It does this by combining the variable name with
 * the line number of the calling (non-macro) code.
 */
#define MACRO_LOCAL(unique_var_name) MACRO_CONCAT(__##unique_var_name##_, __LINE__)

#define ARRAY_SIZE(array) (sizeof(array) / sizeof((array)[0]))

/*
 * macro_noreturn() is a function that just loops forever and never returns.
 */
_Noreturn __attribute__((unused)) static void macro_noreturn()
{
    while (1)
        ;
}

/**
 * macro_unreachable() should be called when the compiler would otherwise complain
 * about a missing return statement.
 */
#ifdef NDEBUG
#define macro_unreachable __builtin_unreachable
#else
#include <assert.h>
#define macro_unreachable()                      \
    do {                                         \
        assert(!"unreachable code got reached"); \
        macro_noreturn();                        \
    } while (0)
#endif
