#include "ipc.h"
#include "../log.h"
#include "pico/multicore.h"
#include "pico/platform.h"
#include <pico/mutex.h>
#include <stdio.h>
#include <string.h>

#define TAG "ipc"

struct shared_store {
    struct mutex mutex;

    uint64_t request_id;
    const struct ipc_request* request;

    uint64_t response_id;
    union ipc_response response;
};

struct response_store {
    struct mutex mutex;
};

static struct shared_store shared_store;
static uint64_t core0_handled_id = 0;

void ipc_init()
{
    mutex_init(&shared_store.mutex);
}

void ipc_start(void (*entry)(void))
{
    multicore_launch_core1(entry);
}

void ipc_stop(void)
{
    hard_assert(get_core_num() == 0);
    if (shared_store.mutex.owner != lock_get_caller_owner_id()) {
        log_debugf(TAG, "mutex_enter_blocking before");
        mutex_enter_blocking(&shared_store.mutex);
        log_debugf(TAG, "mutex_enter_blocking after");
    }

    multicore_reset_core1();

    mutex_exit(&shared_store.mutex);
}

void ipc_send(union ipc_response* ret_resp, const struct ipc_request* req)
{
    hard_assert(get_core_num() == 1);

    mutex_enter_blocking(&shared_store.mutex);
    shared_store.request_id++;
    shared_store.request = req;
    mutex_exit(&shared_store.mutex);

    bool received_response = false;
    while (!received_response) {
        mutex_enter_blocking(&shared_store.mutex);
        if (shared_store.request_id == shared_store.response_id) {
            memcpy(ret_resp, &shared_store.response, sizeof(union ipc_response));
            received_response = true;
        }
        mutex_exit(&shared_store.mutex);
    }
}

const struct ipc_request* ipc_recv()
{
    hard_assert(get_core_num() == 0);

    if (mutex_try_enter(&shared_store.mutex, NULL)) {
        if (shared_store.request_id != core0_handled_id) {
            return shared_store.request;
        }

        mutex_exit(&shared_store.mutex);
    }

    return NULL;
}

void ipc_reply(const struct ipc_request** request, const union ipc_response* resp)
{
    hard_assert(get_core_num() == 0);
    hard_assert(shared_store.mutex.owner == lock_get_caller_owner_id());

    memcpy(&shared_store.response, resp, sizeof(union ipc_response));
    core0_handled_id = shared_store.request_id;
    shared_store.response_id = shared_store.request_id;

    // Prevent accidental use after release
    *request = NULL;

    mutex_exit(&shared_store.mutex);
}
