#pragma once
#include "../control/forward.h"
#include "../drivers/ir.h"

typedef enum { IPC_VOID } ipc_void_t;

enum ipc_op {
    IPC_OP_PING,
    IPC_OP_GET_IR,
    IPC_OP_GET_GYRO,
    IPC_OP_ROTATE_TO,
    IPC_OP_FORWARD,
    IPC_OP_STEP_BACK,
    IPC_OP_SAVE_MAZE,
    IPC_OP_LOAD_MAZE,
};

// core1 -> core0
struct ipc_request {
    enum ipc_op op;
    union {
        int ping;
        ipc_void_t get_ir;
        ipc_void_t get_gyro;
        double rotate_to;
        struct {
            double angle;
            enum forward_stop_cond stop_cond;
        } forward;
        ipc_void_t step_back;
        const struct maze* save_maze;
        struct maze* load_maze;
    } data;
};

// core0 -> core1
union ipc_response {
    int ping;
    struct ir_distances get_ir;
    double get_gyro;
    ipc_void_t rotate_to;
    struct forward_result forward;
    ipc_void_t step_back;
    ipc_void_t save_maze;
    ipc_void_t load_maze;
};

void ipc_init();

void ipc_start(void (*entry)(void));
void ipc_stop(void);

void ipc_send(union ipc_response* ret_resp, const struct ipc_request* req);

const struct ipc_request* ipc_recv();
void ipc_reply(const struct ipc_request** request, const union ipc_response* resp);
