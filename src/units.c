#include "units.h"
#include "lib/macro.h"
#include <assert.h>

enum car_dir car_dir_rotate(enum car_dir car_dir, int rotation)
{
    static_assert(CAR_DIR_MAX == 4, "car_dir_rotate expects CAR_DIR_MAX to be 4");
    assert(car_dir < CAR_DIR_MAX);

    // We use & 0b11 instead of % 4 since the module operator lets negative
    // values stay negative, instead of "wraping around" to the positive range,
    // as desired here.
    return (car_dir - rotation) & 0b11;
}

struct vector car_dir_to_vector(enum car_dir car_dir)
{
    assert(car_dir < CAR_DIR_MAX);

    switch (car_dir) {
    case CAR_DIR_NORTH:
        return (struct vector) { -1, 0 };
    case CAR_DIR_EAST:
        return (struct vector) { 0, 1 };
    case CAR_DIR_SOUTH:
        return (struct vector) { 1, 0 };
    case CAR_DIR_WEST:
        return (struct vector) { 0, -1 };
    }

    macro_unreachable();
}

struct vector vector_add(struct vector a, struct vector b)
{
    return (struct vector) { a.y + b.y, a.x + b.x };
}

bool vector_equal(struct vector a, struct vector b)
{
    return a.x == b.x && a.y == b.y;
}
