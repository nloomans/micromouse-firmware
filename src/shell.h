#pragma once
#include "drivers/button.h"
#include <stdbool.h>

/**
 * shell_process_t represents an active process launched by the shell. It is
 * running a command registered with shell_register().
 */
typedef struct shell_process shell_process_t;

/**
 * shell_function_t the sigure that is to be implemented by a shell command. It
 * is expected to return on completion. If the shell command is long-running,
 * it should monitor for CTRL+C presses using shell_wait() or
 * shell_should_exit().
 */
typedef void (*shell_function_t)(shell_process_t* process, int argc, char** argv);

/**
 * shell_command must always be allocated statically. It contains the
 * definitions of a shell command.
 */
struct shell_command {
    /** The name of the command. */
    const char* name;
    /** The description of the command. Used by the help command. */
    const char* description;
    /** The function implementing the command. */
    shell_function_t func;
    /** Private data that is giving along with the command. Can be accessed using shell_data(). */
    void* data;

    /** A link to the next shell command. Should only be touched by the shell itself. */
    const struct shell_command* next;
};

/**
 * Must be called before any other shell function is executed.
 */
void shell_init(void);

/**
 * shell_wait suspends until the user wants to stop the process using CTRL+C.
 */
void shell_wait(shell_process_t* process);

/**
 * shell_should_exit returns true if the user has requested the process to stop
 * using CTRL+C.
 */
bool shell_should_exit(shell_process_t* process);

/**
 * shell_data returns the .data member of struct shell_command.
 */
void* shell_data(shell_process_t* process);

/**
 * shell_register_struct registers a new shell command. It should called in the
 * init function of a module that wishes to register a shell command.
 *
 * The given command must be statically allocated. The shell assumes complete
 * ownership of the command, it may not be modified after the command is
 * registered.
 *
 * Most users are recommended to use shell_register() instead.
 */
void shell_register_struct(struct shell_command* command);

/**
 * shell_register registers a new shell command. It should called in the init
 * function of a module that wishes to register a shell command.
 *
 * cmd_name: What the user needs to type in the shell to run the command
 * cmd_description: Shown together with your command when running "help"
 * cmd_func: Function that will be called to execute the command.
 *           Must be of type shell_function_t.
 */
#define shell_register(cmd_name, cmd_description, cmd_func)    \
    static struct shell_command __shell_command_##cmd_func = { \
        .name = cmd_name,                                      \
        .description = cmd_description,                        \
        .func = cmd_func,                                      \
    };                                                         \
                                                               \
    shell_register_struct(&__shell_command_##cmd_func)

/**
 * shell_register_shortcut makes pressing the chosen button trigger the command.
 *
 * button: The button to use for this command.
 * command: A string in RO memory that represents the command to execute. Command arguments are supported.
 */
void shell_register_shortcut(enum button button, const char* command);
