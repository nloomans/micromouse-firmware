#pragma once
#include "maze.h"
#include "units.h"
#include <stdbool.h>
#include <stddef.h>

#define RUN_MAX_EVENTS 2048

enum run_event_type {
    RUN_EVENT_CAPTURE,
    RUN_EVENT_ROTATE,
    RUN_EVENT_FORWARD,
};

struct run_event_capture {
    bool wall_present[DIR_MAX];
};

struct run_event_rotate {
    int rotation;
};

struct run_event_forward {
    int squares;
};

struct run_event {
    enum run_event_type type;
    union {
        struct run_event_capture capture;
        struct run_event_rotate rotate;
        struct run_event_forward forward;
    } data;
};

struct run {
    struct maze* maze;
    struct run_event events[RUN_MAX_EVENTS];
    size_t events_len;
    struct run_event event_queue[MAZE_SQUARES];
    size_t event_queue_len;

    struct vector coord;
    enum car_dir car_dir;
};

void run_init(struct run* run, struct maze* maze);
int run_submit_event(struct run* run, struct run_event* event);
