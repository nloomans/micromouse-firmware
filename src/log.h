#pragma once

#include <stdio.h>

void log_init();
void log_clear();
void log_vprintf(const char* fmt, va_list args);
void log_printf(const char* fmt, ...) __attribute__((format(printf, 1, 2)));
void log_stampf(const char* tag, const char* fmt, ...) __attribute__((format(printf, 2, 3)));

#define log_debugf(tag, ...) log_stampf(tag, "   " __VA_ARGS__)
#define log_infof(tag, ...) log_stampf(tag, "-> " __VA_ARGS__)
#define log_errorf(tag, ...) log_stampf(tag, "!! " __VA_ARGS__)

#define log_debugf_slow(cooldown_period_ms, ...)                 \
    ({                                                           \
        static absolute_time_t cooldown = 0;                     \
        if (time_reached(cooldown)) {                            \
            log_debugf(__VA_ARGS__);                             \
            cooldown = make_timeout_time_ms(cooldown_period_ms); \
        }                                                        \
    })
