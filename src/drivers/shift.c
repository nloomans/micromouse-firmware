#include "shift.h"
#include "pins.h"
#include <hardware/gpio.h>
#include <stdint.h>

static uint32_t values = 0;

void shift_init()
{
    gpio_init(PIN_SHIFT_RCLK);
    gpio_init(PIN_SHIFT_SRCLK);
    gpio_init(PIN_SHIFT_SER);
    gpio_set_dir(PIN_SHIFT_RCLK, GPIO_OUT);
    gpio_set_dir(PIN_SHIFT_SRCLK, GPIO_OUT);
    gpio_set_dir(PIN_SHIFT_SER, GPIO_OUT);
    gpio_put(PIN_SHIFT_RCLK, 0);
    gpio_put(PIN_SHIFT_SRCLK, 0);
    gpio_put(PIN_SHIFT_SER, 0);
}

void shift_put(uint32_t spin, bool value)
{
    uint32_t mask = 1 << spin;
    values = (values & ~mask) | (value << spin);
}

void shift_flush()
{
    for (int i = 8 * 3 - 1; i >= 0; i--) {
        gpio_put(PIN_SHIFT_SRCLK, 0);
        gpio_put(PIN_SHIFT_SER, (values >> i) & 1);
        gpio_put(PIN_SHIFT_SRCLK, 1);
    }

    gpio_put(PIN_SHIFT_RCLK, 1);
    gpio_put(PIN_SHIFT_RCLK, 0);
}
