#pragma once

enum button {
    BUTTON_1 = 1 << 0,
    BUTTON_2 = 1 << 1,
    BUTTON_3 = 1 << 2,
    BUTTON_4 = 1 << 3,
};

#define BUTTON_COUNT 4

void button_init();
enum button button_read();

int button_to_index(enum button button);

enum button button_from_index(int index);
