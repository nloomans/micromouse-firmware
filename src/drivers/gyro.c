#include "gyro.h"
#include "../io/i2c_dev.h"
#include "../log.h"
#include "../shell_compat.h"
#include "mpu6500.h"
#include "pins.h"
#include "shift.h"
#include <hardware/gpio.h>
#include <pico/time.h>
#include <pico/types.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define TAG "gyro"

enum taint {
    TAINT_IO_ERROR = 1 << 0,
    TAINT_FIFO_OVERFLOW = 1 << 1,
    TAINT_OUT_OF_BOUNDS = 1 << 2,
};

static const char* taint_to_string(enum taint taint)
{
    const char* str
        = taint == TAINT_IO_ERROR      ? "IO_ERROR"
        : taint == TAINT_FIFO_OVERFLOW ? "FIFO_OVERFLOW"
        : taint == TAINT_OUT_OF_BOUNDS ? "OUT_OF_BOUNDS"
                                       : "<unknown>";

    return str;
}

struct gyro_dps {
    double x, y, z;
};

static struct i2c_dev mpu6500 = {
    .i2c = i2c1,
    .addr = I2C_MPU6500,
};

static float dps = 0;
static float rotation = 0;
static enum taint taint = 0;

static void add_taint(enum taint new_taint)
{
    if ((taint & new_taint) == 0) {
        log_errorf(TAG, "got tainted with taint %s", taint_to_string(new_taint));
    }

    taint |= new_taint;

    shift_put(SPIN_LED_2, 1);
    shift_flush();
}

static float gyro_resolution(enum mpu6500_gyro_fs fs)
{
    return (float)mpu6500_gyro_fs_value(fs) / INT16_MAX;
}

static struct gyro_dps gyro_deg_per_sec(const struct mpu6500_gyro_out* gyro_out, enum mpu6500_gyro_fs fs)
{
    return (struct gyro_dps) {
        .x = gyro_out->x * gyro_resolution(fs),
        .y = gyro_out->y * gyro_resolution(fs),
        .z = gyro_out->z * gyro_resolution(fs),
    };
}

void cmd_print(shell_compat_process_t* process, int argc, char** argv)
{
    static enum mpu6500_gyro_fs fs;
    static int sample_rate;
    static enum mpu6500_dlpf dlpf;
    static float rotation;

    if (shell_compat_iteration(process) == 0) {
        if (argc < 4) {
            printf("Usage: %s <fs> <sample rate> <dlpf>\n", argv[0]);
            printf("\n");
            printf("Arguments:\n");
            printf("  <fs>           One of 250 °/s, 500 °/s, 1000 °/s, 2000 °/s.\n");
            printf("  <sample rate>  Sensor data output rate in Hz.\n");
            printf("  <dlpf>         Digital Low Pass Filter Config, use a value between 1 and 6.\n");
            shell_compat_exit(process);
            return;
        }

        switch (atoi(argv[1])) {
        case 250:
            fs = MPU6500_GYRO_FS_250DPS;
            break;
        case 500:
            fs = MPU6500_GYRO_FS_500DPS;
            break;
        case 1000:
            fs = MPU6500_GYRO_FS_1000DPS;
            break;
        case 2000:
            fs = MPU6500_GYRO_FS_2000DPS;
            break;
        default:
            printf("Invalid FS\n");
            shell_compat_exit(process);
            return;
        }
        mpu6500_set_gyro_fs(&mpu6500, fs);

        sample_rate = atoi(argv[2]);
        mpu6500_set_sample_rate_hz(&mpu6500, sample_rate);

        dlpf = atoi(argv[3]);
        mpu6500_set_digital_low_pass_filter(&mpu6500, dlpf);

        rotation = 0;
    }

    absolute_time_t timeout = make_timeout_time_ms(1000 / sample_rate);

    struct mpu6500_gyro_out gyro_out;
    mpu6500_get_gyro_out(&mpu6500, &gyro_out, false);

    if (mpu6500.error) {
        printf("failed to get sensor data: %d\n", mpu6500.error);
        shell_compat_exit(process);
        return;
    }

    if (gyro_out.z == INT16_MAX || gyro_out.z == INT16_MIN) {
        printf("moved too fast!\n");
        shell_compat_exit(process);
        return;
    }

    struct gyro_dps gyro_dps = gyro_deg_per_sec(&gyro_out, fs);
    rotation += (gyro_dps.z / sample_rate);

    printf("* x (°/s): %7.2f, y (°/s): %7.2f, z (°/s): %7.2f, rotation (°): %7.2f\n", gyro_dps.x, gyro_dps.y, gyro_dps.z, rotation);

    if (time_reached(timeout)) {
        printf("could not keep up!\n");
        shell_compat_exit(process);
        return;
    }
    sleep_until(timeout);
}

void gyro_calibrate()
{
    uint16_t old_sample_rate = mpu6500_get_sample_rate_hz(&mpu6500);
    enum mpu6500_gyro_fs old_gyro_fs = mpu6500_get_gyro_fs(&mpu6500);
    enum mpu6500_dlpf old_dlpf = mpu6500_get_digital_low_pass_filter(&mpu6500);

    mpu6500_set_sample_rate_hz(&mpu6500, 1000);
    mpu6500_set_gyro_fs(&mpu6500, MPU6500_GYRO_FS_250DPS);
    mpu6500_set_digital_low_pass_filter(&mpu6500, MPU6500_DLPF_184_HZ);

    mpu6500_set_fifo_enabled(&mpu6500, true);
    mpu6500_set_fifo_config(&mpu6500,
        MPU6500_FIFO_EN_GYRO_XOUT | MPU6500_FIFO_EN_GYRO_YOUT | MPU6500_FIFO_EN_GYRO_ZOUT);

    struct mpu6500_gyro_offsets zero_offsets = { 0 };
    mpu6500_set_gyro_offsets(&mpu6500, &zero_offsets);

    sleep_ms(200); // wait for sensors to stabalzie

    mpu6500_fifo_reset(&mpu6500);
    mpu6500_read_interrupt_status(&mpu6500); // Clear FIFO overflow status

    struct mpu6500_gyro_out gyro_out = { 0 };
    int total_x = 0, total_y = 0, total_z = 0, sample_size = 0;

    while (sample_size < 2000) {
        uint16_t fifo_count = mpu6500_fifo_get_count(&mpu6500);
        if (fifo_count < sizeof(gyro_out))
            continue;

        mpu6500_get_gyro_out(&mpu6500, &gyro_out, true);
        total_x += gyro_out.x;
        total_y += gyro_out.y;
        total_z += gyro_out.z;
        sample_size++;
    }

    mpu6500_set_fifo_config(&mpu6500, 0);

    struct mpu6500_gyro_offsets offsets = {
        .x = -((total_x / sample_size) >> (MPU6500_GYRO_FS_1000DPS - MPU6500_GYRO_FS_250DPS)),
        .y = -((total_y / sample_size) >> (MPU6500_GYRO_FS_1000DPS - MPU6500_GYRO_FS_250DPS)),
        .z = -((total_z / sample_size) >> (MPU6500_GYRO_FS_1000DPS - MPU6500_GYRO_FS_250DPS)),
    };

    log_debugf(TAG, "calibration finished: x: %d, y: %d, z: %d", offsets.x, offsets.y, offsets.z);
    mpu6500_set_gyro_offsets(&mpu6500, &offsets);

    mpu6500_set_sample_rate_hz(&mpu6500, old_sample_rate);
    mpu6500_set_gyro_fs(&mpu6500, old_gyro_fs);
    mpu6500_set_digital_low_pass_filter(&mpu6500, old_dlpf);

    if (mpu6500.error) {
        add_taint(TAINT_IO_ERROR);
        return;
    }
}

static void cmd_reset_calibration(shell_compat_process_t* process, int argc, char** argv)
{
    struct mpu6500_gyro_offsets zero_offsets = { 0 };
    mpu6500_set_gyro_offsets(&mpu6500, &zero_offsets);

    shell_compat_exit(process);
}

static void cmd_calibrate(shell_compat_process_t* process, int argc, char** argv)
{
    gyro_calibrate();
    shell_compat_exit(process);
}

static void cmd_taint(shell_compat_process_t* process, int argc, char** argv)
{
    if ((taint & TAINT_IO_ERROR) != 0) {
        printf("active taint: IO_ERROR\n");
    }
    if ((taint & TAINT_FIFO_OVERFLOW) != 0) {
        printf("active taint: FIFO_OVERFLOW\n");
    }
    if ((taint & TAINT_OUT_OF_BOUNDS) != 0) {
        printf("active taint: OUT_OF_BOUNDS\n");
    }
    shell_compat_exit(process);
}

void gyro_start()
{
    mpu6500_set_fifo_enabled(&mpu6500, true);
    mpu6500_set_fifo_config(&mpu6500,
        MPU6500_FIFO_EN_GYRO_XOUT | MPU6500_FIFO_EN_GYRO_YOUT | MPU6500_FIFO_EN_GYRO_ZOUT);
    mpu6500_fifo_reset(&mpu6500);
    mpu6500_read_interrupt_status(&mpu6500); // Clear FIFO overflow status
    dps = 0;
    rotation = 0;
}

void gyro_tick()
{
    if (mpu6500_read_interrupt_status(&mpu6500) & MPU6500_INT_FIFO_OVERFLOW) {
        add_taint(TAINT_FIFO_OVERFLOW);
    }

    struct mpu6500_gyro_out gyro_out;
    uint16_t fifo_count = mpu6500_fifo_get_count(&mpu6500);

    for (int i = 0; i < fifo_count; i += sizeof(struct mpu6500_gyro_out)) {
        mpu6500_get_gyro_out(&mpu6500, &gyro_out, true);
        if (mpu6500.error) {
            add_taint(TAINT_IO_ERROR);
            return;
        }

        if (gyro_out.x == INT16_MIN || gyro_out.x == INT16_MAX
            || gyro_out.y == INT16_MIN || gyro_out.y == INT16_MAX
            || gyro_out.z == INT16_MIN || gyro_out.z == INT16_MAX) {
            add_taint(TAINT_OUT_OF_BOUNDS);
        }

        struct gyro_dps gyro_dps = gyro_deg_per_sec(&gyro_out, MPU6500_GYRO_FS_1000DPS);
        dps = gyro_dps.z;
        rotation += (gyro_dps.z / 1000);
    }
}

float gyro_rotation()
{
    return rotation;
}

float gyro_dps()
{
    return dps;
}

void gyro_init()
{
    i2c_init(i2c1, 400 * 1000);
    gpio_set_function(PIN_I2C1_SDA, GPIO_FUNC_I2C);
    gpio_set_function(PIN_I2C1_SDL, GPIO_FUNC_I2C);
    gpio_pull_up(PIN_I2C1_SDA);
    gpio_pull_up(PIN_I2C1_SDL);

    if (!mpu6500_exists(&mpu6500)) {
        log_errorf(TAG, "could not find MPU-6500");
        add_taint(TAINT_IO_ERROR);
        return;
    }

    mpu6500_reset(&mpu6500);

    mpu6500_set_sample_rate_hz(&mpu6500, 1000);
    mpu6500_set_gyro_fs(&mpu6500, MPU6500_GYRO_FS_1000DPS);
    mpu6500_set_digital_low_pass_filter(&mpu6500, MPU6500_DLPF_184_HZ);

    shell_compat_register("gyro-print", "Print sensor values and rotation", cmd_print);
    shell_compat_register("gyro-calibrate", "Recalibrate the sensor", cmd_calibrate);
    shell_compat_register("gyro-reset", "Reset calibration of the sensor", cmd_reset_calibration);
    shell_compat_register("gyro-taint", "List active taints", cmd_taint);

    if (mpu6500.error) {
        log_errorf(TAG, "configuration failed: %d", mpu6500.error);
        add_taint(TAINT_IO_ERROR);
        return;
    }
}
