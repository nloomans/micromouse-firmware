#include "button.h"
#include "../shell_compat.h"
#include "pico/time.h"
#include "pico/types.h"
#include "pins.h"
#include "shift.h"
#include <hardware/gpio.h>
#include <stdbool.h>
#include <stdio.h>

#define BUTTON_ALL (BUTTON_1 | BUTTON_2 | BUTTON_3 | BUTTON_4)

static void mutex_configure(enum button buttons)
{
    shift_put(SPIN_BUTTON_1, buttons & BUTTON_1);
    shift_put(SPIN_BUTTON_2, buttons & BUTTON_2);
    shift_put(SPIN_BUTTON_3, buttons & BUTTON_3);
    shift_put(SPIN_BUTTON_4, buttons & BUTTON_4);
    shift_flush();
}

enum button button_read()
{
    unsigned buttons_in = gpio_get(PIN_BUTTONS_IN);
    if (!buttons_in) {
        return 0;
    }

    enum button buttons_pressed = 0;

    for (int i = 0; i < BUTTON_COUNT; i++) {
        enum button button = button_from_index(i);

        mutex_configure(button);

        if (gpio_get(PIN_BUTTONS_IN)) {
            buttons_pressed |= button;
        }
    }

    mutex_configure(BUTTON_ALL);

    return buttons_pressed;
}

int button_to_index(enum button button)
{
    switch (button) {
    case BUTTON_1:
        return 0;
    case BUTTON_2:
        return 1;
    case BUTTON_3:
        return 2;
    case BUTTON_4:
        return 3;
    default:
        return -1;
    }
}

enum button button_from_index(int index)
{
    return 1 << index;
}

void cmd_test(shell_compat_process_t* process, int argc, char** argv)
{
    static enum button prev_buttons_pressed = 0;
    if (shell_compat_iteration(process) == 0) {
        prev_buttons_pressed = 0;
    }

    enum button buttons_pressed = button_read();

    if (buttons_pressed != prev_buttons_pressed) {
        printf("[%12llu] buttons pressed: [%c] [%c] [%c] [%c]\n",
            to_us_since_boot(get_absolute_time()),
            (buttons_pressed & BUTTON_1) ? 'X' : ' ',
            (buttons_pressed & BUTTON_2) ? 'X' : ' ',
            (buttons_pressed & BUTTON_3) ? 'X' : ' ',
            (buttons_pressed & BUTTON_4) ? 'X' : ' ');
    }

    prev_buttons_pressed = buttons_pressed;
}

void button_init()
{
    mutex_configure(BUTTON_ALL);
    gpio_pull_up(PIN_BUTTONS_IN);

    shell_compat_register("button-read", "Read all buttons", cmd_test);
}
