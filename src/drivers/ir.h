#pragma once
#include <stdint.h>

enum ir_sensor {
    IR_LEFT,
    IR_FRONT_LEFT,
    IR_FRONT,
    IR_FRONT_RIGHT,
    IR_RIGHT,
};

#define IR_SENSOR_COUNT 5

struct ir_distances {
    double sensor_mm[IR_SENSOR_COUNT];
    uint64_t capture_cycle;
};

void ir_init();
void ir_tick();
int ir_read(struct ir_distances* ret_distances);
