#include "mpu6500.h"
#include "../io/i2c_dev.h"
#include <machine/endian.h>
#include <stdio.h>

#define REG_XG_OFFSET_H 0x13
#define REG_SMPLRT_DIV 0x19
#define REG_CONFIG 0x1A
#define REG_GYRO_CONFIG 0x1B
#define REG_FIFO_EN 0x23
#define REG_INT_ENABLE 0x38
#define REG_INT_STATUS 0x3A
#define REG_GYRO_XOUT_H 0x43
#define REG_FIFO_COUNT_H 0x72
#define REG_FIFO_R_W 0x74
#define REG_USER_CTRL 0x6A
#define REG_PWR_MGMT_1 0x6B
#define REG_WHO_AM_I 0x75

#define GYRO_OFFSETS_START REG_XG_OFFSET_H
#define DLPF_CFG I2C_DEV_BITS(REG_CONFIG, 0, 2)
#define GYRO_FS_SEL I2C_DEV_BITS(REG_GYRO_CONFIG, 3, 2)
#define GYRO_OUT_START REG_GYRO_XOUT_H
#define FIFO_RST I2C_DEV_BIT(REG_USER_CTRL, 2)
#define FIFO_EN I2C_DEV_BIT(REG_USER_CTRL, 6)
#define DEVICE_RESET I2C_DEV_BIT(REG_PWR_MGMT_1, 7)

bool mpu6500_exists(struct i2c_dev* dev)
{
    return i2c_dev_read_byte(dev, REG_WHO_AM_I) == 0x70;
}

void mpu6500_reset(struct i2c_dev* dev)
{
    i2c_dev_write_bit(dev, DEVICE_RESET, true);

    // We should wait 100ms after issuing a reset command, see page 42
    sleep_ms(100);
}

enum mpu6500_int mpu6500_get_interrupt_enable(struct i2c_dev* dev)
{
    return i2c_dev_read_byte(dev, REG_INT_ENABLE);
}

void mpu6500_set_interrupt_enable(struct i2c_dev* dev, enum mpu6500_int interrupt)
{
    i2c_dev_write_byte(dev, REG_INT_ENABLE, interrupt);
}

enum mpu6500_int mpu6500_read_interrupt_status(struct i2c_dev* dev)
{
    return i2c_dev_read_byte(dev, REG_INT_STATUS);
}

uint16_t mpu6500_get_sample_rate_hz(struct i2c_dev* dev)
{
    uint16_t internal_sample_rate = 1000;
    uint8_t divider = i2c_dev_read_byte(dev, REG_SMPLRT_DIV);

    return internal_sample_rate / (1 + divider);
}

void mpu6500_set_sample_rate_hz(struct i2c_dev* dev, uint16_t rate)
{
    uint16_t internal_sample_rate = 1000;
    uint16_t divider = internal_sample_rate / rate - 1;
    uint16_t final_rate = internal_sample_rate / (1 + divider);
    if (rate != final_rate) {
        printf("[mpu6500] sample rate %d Hz became %d Hz\n", rate, final_rate);
    }

    i2c_dev_write_byte(dev, REG_SMPLRT_DIV, divider);
}

enum mpu6500_gyro_fs mpu6500_get_gyro_fs(struct i2c_dev* dev)
{
    return i2c_dev_read_bits(dev, GYRO_FS_SEL);
}

void mpu6500_set_gyro_fs(struct i2c_dev* dev, enum mpu6500_gyro_fs fs)
{
    i2c_dev_write_bits(dev, GYRO_FS_SEL, fs);
}

void mpu6500_get_gyro_out(struct i2c_dev* dev, struct mpu6500_gyro_out* ret_gyro_out, bool fifo)
{
    struct mpu6500_gyro_out gyro_out_be;

    uint8_t reg = fifo ? REG_FIFO_R_W : GYRO_OUT_START;
    i2c_dev_read(dev, reg, &gyro_out_be, sizeof(gyro_out_be));
    if (dev->error)
        return;

    *ret_gyro_out = (struct mpu6500_gyro_out) {
        .x = __bswap16(gyro_out_be.x),
        .y = __bswap16(gyro_out_be.y),
        .z = __bswap16(gyro_out_be.z),
    };
}

void mpu6500_get_gyro_offsets(struct i2c_dev* dev, struct mpu6500_gyro_offsets* ret_offsets)
{
    struct mpu6500_gyro_offsets offsets_be;

    i2c_dev_read(dev, GYRO_OFFSETS_START, &offsets_be, sizeof(offsets_be));
    if (dev->error)
        return;

    *ret_offsets = (struct mpu6500_gyro_offsets) {
        .x = __bswap16(offsets_be.x),
        .y = __bswap16(offsets_be.y),
        .z = __bswap16(offsets_be.z),
    };
}

void mpu6500_set_gyro_offsets(struct i2c_dev* dev, const struct mpu6500_gyro_offsets* offsets)
{
    struct mpu6500_gyro_offsets offsets_be = {
        .x = __bswap16(offsets->x),
        .y = __bswap16(offsets->y),
        .z = __bswap16(offsets->z),
    };

    i2c_dev_write(dev, GYRO_OFFSETS_START, &offsets_be, sizeof(offsets_be));
}

enum mpu6500_fifo_en mpu6500_get_fifo_config(struct i2c_dev* dev)
{
    return i2c_dev_read_byte(dev, REG_FIFO_EN);
}

void mpu6500_set_fifo_config(struct i2c_dev* dev, enum mpu6500_fifo_en fifo_en)
{
    i2c_dev_write_byte(dev, REG_FIFO_EN, fifo_en);
}

bool mpu6500_get_fifo_enabled(struct i2c_dev* dev)
{
    return i2c_dev_read_bit(dev, FIFO_EN);
}

void mpu6500_set_fifo_enabled(struct i2c_dev* dev, bool enabled)
{
    i2c_dev_write_bit(dev, FIFO_EN, enabled);
}

void mpu6500_fifo_reset(struct i2c_dev* dev)
{
    i2c_dev_write_bit(dev, FIFO_RST, true);
}

uint16_t mpu6500_fifo_get_count(struct i2c_dev* dev)
{
    return __bswap16(i2c_dev_read_short(dev, REG_FIFO_COUNT_H));
}

void mpu6500_fifo_read(struct i2c_dev* dev, void* dst, size_t size)
{
    return i2c_dev_read(dev, REG_FIFO_R_W, dst, size);
}

enum mpu6500_dlpf mpu6500_get_digital_low_pass_filter(struct i2c_dev* dev)
{
    return i2c_dev_read_bits(dev, DLPF_CFG);
}

void mpu6500_set_digital_low_pass_filter(struct i2c_dev* dev, enum mpu6500_dlpf dlpf)
{
    i2c_dev_write_bits(dev, DLPF_CFG, dlpf);
}

uint16_t mpu6500_gyro_fs_value(enum mpu6500_gyro_fs fs)
{
    return 250 << fs;
}
