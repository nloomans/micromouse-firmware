#pragma once

#define PIN_I2C0_SDA 0
#define PIN_I2C0_SDL 1
#define PIN_I2C1_SDA 2
#define PIN_I2C1_SDL 3
#define PIN_MPU_FSYNC 4
#define PIN_MPU_INT 5

#define PIN_MOTOR_L_IN1 14 // R and L are wrong in schematic, but also reversed in connector.
#define PIN_MOTOR_L_IN2 15
#define PIN_MOTOR_R_IN1 12
#define PIN_MOTOR_R_IN2 13

#define PIN_ENCODER_R 7
#define PIN_ENCODER_L 8

#define PIN_SHIFT_RCLK 9 // Storage register clock
#define PIN_SHIFT_SRCLK 10 // Shift register clock
#define PIN_SHIFT_SER 11 // Shift register serial input
#define PIN_IR_TX_CLK 6 // Output enable of the 2nd shift reg

#define PIN_BUTTONS_IN 24

#define PIN_BUZZER 25

#define PIN_IR_RX1 27
#define PIN_IR_RX2 26

#define PIN_MAX 29

#define SPIN(registry, pin) ((registry * 010) + (pin - 'A'))

#define SPIN_LIDAR_SELECT_0 SPIN(0, 'D')
#define SPIN_LIDAR_SELECT_1 SPIN(0, 'C')
#define SPIN_LIDAR_SELECT_2 SPIN(0, 'B')

#define SPIN_FILTER_SELECT_0 SPIN(0, 'F')
#define SPIN_FILTER_SELECT_1 SPIN(0, 'G')
#define SPIN_FILTER_SELECT_2 SPIN(0, 'H')

#define SPIN_IR_TX1 SPIN(1, 'G')
#define SPIN_IR_TX2 SPIN(1, 'E')
#define SPIN_IR_TX3 SPIN(1, 'F')
#define SPIN_IR_TX4 SPIN(1, 'H')
#define SPIN_IR_TX5 SPIN(1, 'D')

#define SPIN_BUTTON_1 SPIN(2, 'A')
#define SPIN_BUTTON_2 SPIN(2, 'B')
#define SPIN_BUTTON_3 SPIN(2, 'C')
#define SPIN_BUTTON_4 SPIN(2, 'D')

#define SPIN_LED_1 SPIN(2, 'E')
#define SPIN_LED_2 SPIN(2, 'F')
#define SPIN_LED_3 SPIN(2, 'G')
#define SPIN_LED_4 SPIN(2, 'H')

#define ADC_IR_RX1 1
#define ADC_IR_RX2 0

#define I2C_MPU6500 0x68
