#include "motor.h"
#include "../interrupt.h"
#include "../log.h"
#include "../shell_compat.h"
#include "button.h"
#include "hardware/timer.h"
#include "pico/types.h"
#include "pins.h"
#include <hardware/gpio.h>
#include <hardware/pwm.h>
#include <pico/time.h>
#include <signal.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TAG "motor"

static volatile sig_atomic_t count[MOTOR_COUNT] = { 0, 0 };

static volatile uint64_t tick_times[MOTOR_COUNT][3]
    = { { 0, 0, 0 }, { 0, 0, 0 } };

static void irq_handler(unsigned int gpio, uint32_t events)
{
    enum motor motor;
    if (gpio == PIN_ENCODER_L) {
        motor = MOTOR_LEFT;
    } else if (gpio == PIN_ENCODER_R) {
        motor = MOTOR_RIGHT;
    } else {
        return;
    }

    count[motor]++;
    tick_times[motor][2] = tick_times[motor][1];
    tick_times[motor][1] = tick_times[motor][0];
    tick_times[motor][0] = to_us_since_boot(get_absolute_time());
}

int motor_distance(enum motor motor)
{
    return count[motor];
}

void motor_set_duty(enum motor motor, enum motor_drive motor_drive, uint16_t duty)
{
    // One motor code for pulsing IN1, IN2 or both
    // Slice A is IN1, Slice B is IN2
    uint slice_motor;
    uint16_t in2 = duty;
    uint16_t in1 = 0;

    if (motor == MOTOR_RIGHT) {
        slice_motor = pwm_gpio_to_slice_num(PIN_MOTOR_R_IN1);
    } else {
        slice_motor = pwm_gpio_to_slice_num(PIN_MOTOR_L_IN1);
        in2 = 0; // Motor right has different direction of gear, reverse left input in code
        in1 = duty;
    }

    if (motor_drive == MOTOR_FORWARD) {
        pwm_set_chan_level(slice_motor, PWM_CHAN_A, in1);
        pwm_set_chan_level(slice_motor, PWM_CHAN_B, in2);
    } else if (motor_drive == MOTOR_BACKWARD) { // For backward reverse inputs
        pwm_set_chan_level(slice_motor, PWM_CHAN_A, in2);
        pwm_set_chan_level(slice_motor, PWM_CHAN_B, in1);
    } else { // Else motor == MOTOR_BREAK, pulse both for speed of braking
        pwm_set_chan_level(slice_motor, PWM_CHAN_A, duty);
        pwm_set_chan_level(slice_motor, PWM_CHAN_B, duty);
    }

    pwm_set_enabled(slice_motor, true);
}

void motor_straight(uint16_t duty)
{
    motor_set_duty(MOTOR_LEFT, MOTOR_FORWARD, duty);
    motor_set_duty(MOTOR_RIGHT, MOTOR_FORWARD, duty);
}

void motor_turn(enum motor turn_dir, uint16_t duty)
{
    if (turn_dir == MOTOR_LEFT) {
        motor_set_duty(MOTOR_LEFT, MOTOR_BACKWARD, duty);
        motor_set_duty(MOTOR_RIGHT, MOTOR_FORWARD, duty);
    } else {
        motor_set_duty(MOTOR_LEFT, MOTOR_FORWARD, duty);
        motor_set_duty(MOTOR_RIGHT, MOTOR_BACKWARD, duty);
    }
}

void motor_off()
{
    motor_straight(0);
}

static void cmd_speed(shell_compat_process_t* process, int argc, char** argv)
{
    if (shell_compat_iteration(process) > 0)
        return;

    shell_compat_set_cleanup_callback(process, &motor_off);

    if (argc != 4) {
        printf("Usage: %s <motor> <drive> <duty>\n", argv[0]);
        printf("\n");
        printf("Arguments:\n");
        printf("  <motor>   'left' or 'right'\n");
        printf("  <drive>   'fwd' for forwards, 'bwd' for backwards, 'break' for breaking\n");
        printf("  <duty>    0x8000 for 50%%, 0xffff for 100%%\n");
        shell_compat_exit(process);
        return;
    }

    enum motor motor;
    if (strcmp(argv[1], "left") == 0) {
        motor = MOTOR_LEFT;
    } else if (strcmp(argv[1], "right") == 0) {
        motor = MOTOR_RIGHT;
    } else {
        printf("Invalid <motor> value\n");
        shell_compat_exit(process);
        return;
    }

    enum motor_drive motor_drive;
    if (strcmp(argv[2], "fwd") == 0) {
        motor_drive = MOTOR_FORWARD;
    } else if (strcmp(argv[2], "bwd") == 0) {
        motor_drive = MOTOR_BACKWARD;
    } else if (strcmp(argv[2], "break") == 0) {
        motor_drive = MOTOR_BREAK;
    } else {
        printf("Invalid <drive> value\n");
        shell_compat_exit(process);
        return;
    }

    long duty = strtol(argv[3], NULL, 0);
    if (duty < 0 && duty > 0xffff) {
        printf("Invalid <duty> value\n");
        shell_compat_exit(process);
        return;
    }

    motor_set_duty(motor, motor_drive, duty);
}

static void cmd_straight_forward(shell_compat_process_t* process, int argc, char** argv)
{
    if (shell_compat_iteration(process) > 0)
        return;

    shell_compat_set_cleanup_callback(process, &motor_off);

    if (argc != 2) {
        printf("Usage: %s <duty>\n", argv[0]);
        printf("\n");
        printf("Arguments:\n");
        printf("  <duty>    0x8000 for 50%%, 0xffff for 100%%\n");
        shell_compat_exit(process);
        return;
    }

    long duty = strtol(argv[1], NULL, 0);
    if (duty < 0 && duty > 0xffff) {
        printf("Invalid <duty> value\n");
        shell_compat_exit(process);
        return;
    }

    motor_straight(duty);
}

void motor_maintain_speed(enum motor motor, double speed_ref)
{
    bool direction = speed_ref > 0;
    if (!direction)
        speed_ref = -speed_ref;

    double kp = 75;
    double kd = -0.001;

    double speed = (4.0 / (double)(tick_times[motor][0] - tick_times[motor][1]))
        * 1 * 1000 * 1000;
    double prev_speed = (4.0 / (double)(tick_times[motor][1] - tick_times[motor][2]))
        * 1 * 1000 * 1000;

    double accel = (speed - prev_speed) / (double)(tick_times[motor][0] - tick_times[motor][1])
        * 1 * 1000 * 1000;

    if (to_us_since_boot(get_absolute_time()) - tick_times[motor][2] > 100000) {
        speed = 0;
        prev_speed = 0;
        accel = 0;
    }

    double error = speed_ref - speed;
    double proportial = kp * error;
    double differential = kd * accel;

    int32_t duty = proportial + differential;

    if (duty > 0xffff)
        duty = 0xffff;

    // log_debugf("motor", "* speed: %10.2f, accel: %10.2f, error: %10.2f, p: %10.2f, d: %10.2f, duty: %.2f",
    //     speed, accel, error, proportial, differential, (double)duty / 0xffff);

    if (duty < 0) {
        if (direction == 1) {
            motor_set_duty(motor, MOTOR_BACKWARD, -duty);
        } else {
            motor_set_duty(motor, MOTOR_FORWARD, -duty);
        }
    } else {
        if (direction == 1) {
            motor_set_duty(motor, MOTOR_FORWARD, duty);
        } else {
            motor_set_duty(motor, MOTOR_BACKWARD, duty);
        }
    }
}

static void cmd_pid(shell_compat_process_t* process, int argc, char** argv)
{
    static absolute_time_t timeout;

    if (shell_compat_iteration(process) == 0) {
        log_clear();
        shell_compat_set_cleanup_callback(process, &motor_off);
        timeout = make_timeout_time_ms(5000);
    }

    motor_maintain_speed(MOTOR_LEFT, 500.0);
    motor_maintain_speed(MOTOR_RIGHT, 500.0);

    if (time_reached(timeout) || button_read() & BUTTON_1) {
        shell_compat_exit(process);
    }
}

static void cmd_print_distance(shell_compat_process_t* process, int argc, char** argv)
{
    static int prev_left, prev_right;
    if (shell_compat_iteration(process) == 0) {
        prev_left = -1;
        prev_right = -1;
    }

    int left = count[MOTOR_LEFT];
    int right = count[MOTOR_RIGHT];
    if (left != prev_left || right != prev_right) {
        printf("left: %5d, right: %5d\n", left, right);
    }

    prev_left = left;
    prev_right = right;
}

void motor_init()
{
    // Run once before unsing motor functions | Turn PWM slices off, attach pico pins to slices
    uint slice_right = pwm_gpio_to_slice_num(PIN_MOTOR_R_IN1); // R_IN1 and R_IN2 use the same slice
    uint slice_left = pwm_gpio_to_slice_num(PIN_MOTOR_L_IN1);
    pwm_set_enabled(slice_right, false);
    pwm_set_enabled(slice_left, false);

    // DRV8212P only allows 0 - 100 kHz | 125 Mhz / 16 bit / 2 = 1 kHz
    pwm_set_clkdiv(slice_right, 2);
    pwm_set_clkdiv(slice_left, 2);

    gpio_set_function(PIN_MOTOR_R_IN1, GPIO_FUNC_PWM);
    gpio_set_function(PIN_MOTOR_R_IN2, GPIO_FUNC_PWM);
    gpio_set_function(PIN_MOTOR_L_IN1, GPIO_FUNC_PWM);
    gpio_set_function(PIN_MOTOR_L_IN2, GPIO_FUNC_PWM);

    gpio_init(PIN_ENCODER_L);
    gpio_set_dir(PIN_ENCODER_L, GPIO_IN);
    gpio_pull_up(PIN_ENCODER_L);

    gpio_init(PIN_ENCODER_R);
    gpio_set_dir(PIN_ENCODER_R, GPIO_IN);
    gpio_pull_up(PIN_ENCODER_R);

    interrupt_gpio_register(PIN_ENCODER_L, GPIO_IRQ_EDGE_FALL | GPIO_IRQ_EDGE_RISE, irq_handler);
    interrupt_gpio_register(PIN_ENCODER_R, GPIO_IRQ_EDGE_FALL | GPIO_IRQ_EDGE_RISE, irq_handler);

    shell_compat_register("motor-speed", "Control motor left or right", cmd_speed);
    shell_compat_register("motor-straight", "Drive both motors forward for a few ms with a given duty cycle", cmd_straight_forward);
    shell_compat_register("motor-print-distance", "Print motor distance", cmd_print_distance);
    shell_compat_register("motor-pid", "", cmd_pid);
}
