#pragma once

void gyro_init();

void gyro_calibrate();

void gyro_start();

void gyro_tick();

float gyro_rotation();
float gyro_dps();
