#pragma once
#include "../io/i2c_dev.h"
#include <stdbool.h>
#include <stdint.h>

enum mpu6500_gyro_fs {
    MPU6500_GYRO_FS_250DPS = 0, //  +/-  250 º/s -> 131 LSB/(º/s)
    MPU6500_GYRO_FS_500DPS = 1, //  +/-  500 º/s -> 65.5 LSB/(º/s)
    MPU6500_GYRO_FS_1000DPS = 2, // +/- 1000 º/s -> 32.8 LSB/(º/s)
    MPU6500_GYRO_FS_2000DPS = 3, // +/- 2000 º/s -> 16.4 LSB/(º/s)
};

enum mpu6500_int {
    MPU6500_INT_RAW_DATA_RDY = 1 << 0,
    MPU6500_INT_DMP = 1 << 1,
    MPU6500_INT_FSYNC = 1 << 3,
    MPU6500_INT_FIFO_OVERFLOW = 1 << 4,
    MPU6500_INT_WOM = 1 << 6,
};

enum mpu6500_dlpf {
    MPU6500_DLPF_250_HZ = 0, // bandwidth: 250 Hz, delay: 0.97 ms, internal sample rate: 8 kHz. mpu6500_set_sample_rate_hz() has no effect with this DLPF!
    MPU6500_DLPF_184_HZ = 1, // bandwidth: 184 Hz, delay: 2.9 ms, internal sample rate: 1 kHz
    MPU6500_DLPF_92_HZ = 2, // bandwidth: 92 Hz, delay: 3.9 ms, internal sample rate: 1 kHz
    MPU6500_DLPF_41_HZ = 3, // bandwidth: 41 Hz, delay: 5.9 ms, internal sample rate: 1 kHz
    MPU6500_DLPF_20_HZ = 4, // bandwidth: 20 Hz, delay: 9.9 ms, internal sample rate: 1 kHz
    MPU6500_DLPF_10_HZ = 5, // bandwidth: 10 Hz, delay: 17.85 ms, internal sample rate: 1 kHz
    MPU6500_DLPF_5_HZ = 6, // bandwith: 5 Hz, delay: 33.48 ms, internal sample rate: 1 kHz
    MPU6500_DLPF_3600_HZ = 7, // bandwidth: 250 Hz, delay: 0.97 ms, internal sample rate: 8 kHz. mpu6500_set_sample_rate_hz() has no effect with this DLPF!
};

enum mpu6500_fifo_en {
    MPU6500_FIFO_EN_SLV_0 = 1 << 0,
    MPU6500_FIFO_EN_SLV_1 = 1 << 1,
    MPU6500_FIFO_EN_SLV_2 = 1 << 2,
    MPU6500_FIFO_EN_ACCEL = 1 << 3,
    MPU6500_FIFO_EN_GYRO_ZOUT = 1 << 4,
    MPU6500_FIFO_EN_GYRO_YOUT = 1 << 5,
    MPU6500_FIFO_EN_GYRO_XOUT = 1 << 6,
    MPU6500_FIFO_EN_TEMP_OUT = 1 << 7,
};

struct mpu6500_gyro_config {
    bool xg_st : 1;
    bool yg_st : 1;
    bool zg_st : 1;
    enum mpu6500_gyro_fs gyro_fs_sel : 2;
    unsigned _reserved : 1;
    unsigned fchoice_b : 2;
} __attribute__((packed));

struct mpu6500_gyro_out {
    int16_t x, y, z;
};

struct mpu6500_gyro_offsets {
    int16_t x, y, z;
};

bool mpu6500_exists(struct i2c_dev* dev);

void mpu6500_reset(struct i2c_dev* dev);

enum mpu6500_int mpu6500_get_interrupt_enable(struct i2c_dev* dev);
void mpu6500_set_interrupt_enable(struct i2c_dev* dev, enum mpu6500_int interrupt);

enum mpu6500_int mpu6500_read_interrupt_status(struct i2c_dev* dev);

uint16_t mpu6500_get_sample_rate_hz(struct i2c_dev* dev);
void mpu6500_set_sample_rate_hz(struct i2c_dev* dev, uint16_t rate);

enum mpu6500_gyro_fs mpu6500_get_gyro_fs(struct i2c_dev* dev);
void mpu6500_set_gyro_fs(struct i2c_dev* dev, enum mpu6500_gyro_fs fs);

void mpu6500_get_gyro_out(struct i2c_dev* dev, struct mpu6500_gyro_out* ret_gyro_out, bool fifo);

void mpu6500_get_gyro_offsets(struct i2c_dev* dev, struct mpu6500_gyro_offsets* ret_offsets);
void mpu6500_set_gyro_offsets(struct i2c_dev* dev, const struct mpu6500_gyro_offsets* offsets);

enum mpu6500_fifo_en mpu6500_get_fifo_config(struct i2c_dev* dev);
void mpu6500_set_fifo_config(struct i2c_dev* dev, enum mpu6500_fifo_en fifo_en);

bool mpu6500_get_fifo_enabled(struct i2c_dev* dev);
void mpu6500_set_fifo_enabled(struct i2c_dev* dev, bool enabled);

void mpu6500_fifo_reset(struct i2c_dev* dev);

uint16_t mpu6500_fifo_get_count(struct i2c_dev* dev);
void mpu6500_fifo_read(struct i2c_dev* dev, void* dst, size_t size);

enum mpu6500_dlpf mpu6500_get_digital_low_pass_filter(struct i2c_dev* dev);
void mpu6500_set_digital_low_pass_filter(struct i2c_dev* dev, enum mpu6500_dlpf dlpf);

uint16_t mpu6500_gyro_fs_value(enum mpu6500_gyro_fs fs);
