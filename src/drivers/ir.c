#include "ir.h"
#include "../shell_compat.h"
#include "pins.h"
#include "shift.h"
#include <assert.h>
#include <hardware/adc.h>
#include <hardware/dma.h>
#include <hardware/pwm.h>
#include <hardware/sync.h>
#include <hardware/timer.h>
#include <math.h>
#include <pico/time.h>
#include <pico/types.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct sensor {
    int rx_filter;
    int tx_spin;
};

const struct sensor sensors[] = {
    [IR_LEFT] = {
        .rx_filter = 3,
        .tx_spin = SPIN_IR_TX4,
    },
    [IR_FRONT_LEFT] = {
        .rx_filter = 0,
        .tx_spin = SPIN_IR_TX1,
    },
    [IR_FRONT] = {
        .rx_filter = 2,
        .tx_spin = SPIN_IR_TX3,
    },
    [IR_FRONT_RIGHT] = {
        .rx_filter = 1,
        .tx_spin = SPIN_IR_TX2,
    },
    [IR_RIGHT] = {
        .rx_filter = 4,
        .tx_spin = SPIN_IR_TX5,
    },
};

struct ir_measurement {
    uint16_t short_range;
    uint16_t long_range;
};

#define ADC_SAMPLE_RATE_HZ 500000
#define IR_PERIOD_DURATION_US 100
#define US_IN_S (1 * 1000 * 1000)

// Weird order to prevent flooring to zero when dividing
#define PERIOD_SAMPLES ((ADC_SAMPLE_RATE_HZ * IR_PERIOD_DURATION_US) / US_IN_S)

// The amount of periods we want to use to look for the peak
#define PERIOD_COUNT 20

// The amount of periods to ignore after switching sensors
#define PERIOD_SKIP 10

#define CAPTURE_BUFFER_SIZE (PERIOD_COUNT * PERIOD_SAMPLES)

uint dma_chan = 0;
bool is_capturing = false;
uint16_t capture_buffer[CAPTURE_BUFFER_SIZE];
uint64_t capture_cycle = 0;

static enum ir_sensor selected_sensor = -1;

static struct ir_measurement measurements[IR_SENSOR_COUNT];

static void select_sensor(enum ir_sensor sensor)
{
    if (selected_sensor == sensor) {
        return;
    }

    // Select the sensor RX that the filter will consume
    int level_shifted = ~sensors[sensor].rx_filter;

    shift_put(SPIN_FILTER_SELECT_0, level_shifted & (1 << 0));
    shift_put(SPIN_FILTER_SELECT_1, level_shifted & (1 << 1));
    shift_put(SPIN_FILTER_SELECT_2, level_shifted & (1 << 2));

    // Select the sensor TX
    for (enum ir_sensor i = 0; i < IR_SENSOR_COUNT; i++) {
        shift_put(sensors[i].tx_spin, i == sensor);
    }

    shift_flush();

    selected_sensor = sensor;
}

static void start_capturing()
{
    assert(is_capturing == false);

    dma_channel_config cfg = dma_channel_get_default_config(dma_chan);

    // Reading from constant address, writing to incrementing byte addresses
    channel_config_set_transfer_data_size(&cfg, DMA_SIZE_16);
    channel_config_set_read_increment(&cfg, false);
    channel_config_set_write_increment(&cfg, true);

    // Pace transfers based on availability of ADC samples
    channel_config_set_dreq(&cfg, DREQ_ADC);

    dma_channel_configure(dma_chan, &cfg,
        capture_buffer,
        &adc_hw->fifo,
        CAPTURE_BUFFER_SIZE,
        true // start immediately
    );

    is_capturing = true;
}

static void stop_capturing()
{
    assert(is_capturing == true);

    dma_channel_cleanup(dma_chan);
    is_capturing = false;
}

static void select_next()
{
    switch (adc_get_selected_input()) {
    case ADC_IR_RX1:
        adc_select_input(ADC_IR_RX2);
        break;
    case ADC_IR_RX2:
        switch (selected_sensor) {
        case IR_LEFT:
            select_sensor(IR_FRONT);
            break;
        case IR_FRONT_LEFT:
            select_sensor(IR_FRONT);
            break;
        case IR_FRONT:
            select_sensor(IR_RIGHT);
            break;
        case IR_FRONT_RIGHT:
            select_sensor(IR_RIGHT);
            break;
        case IR_RIGHT:
            select_sensor(IR_LEFT);
            break;
        }

        adc_select_input(ADC_IR_RX1);

        if (selected_sensor == 0) {
            capture_cycle++;
        }
        break;
    }
}

void ir_tick()
{
    if (!is_capturing) {
        start_capturing();
        return;
    }

    if (dma_channel_is_busy(dma_chan)) {
        return;
    }

    uint16_t valleys[PERIOD_COUNT];
    for (int period = 0; period < PERIOD_COUNT; period++) {
        valleys[period] = UINT16_MAX;
        for (int sample = 0; sample < PERIOD_SAMPLES; sample++) {
            int offset = (period * PERIOD_SAMPLES) + sample;

            if (capture_buffer[offset] < valleys[period]) {
                valleys[period] = capture_buffer[offset];
            }
        }
    }

    uint16_t minimum = UINT16_MAX;
    for (int period = PERIOD_SKIP; period < PERIOD_COUNT; period++) {
        if (valleys[period] < minimum) {
            minimum = valleys[period];
        }
    }

    switch (adc_get_selected_input()) {
    case ADC_IR_RX1:
        measurements[selected_sensor].short_range = minimum;
        break;
    case ADC_IR_RX2:
        measurements[selected_sensor].long_range = minimum;
        break;
    }

    stop_capturing();
    select_next();
    start_capturing();
}

static double short_range_to_mm(uint16_t short_range)
{
    double shift = 70;
    double steepness = 1.15;
    double y_offset = 3640;

    return shift - log(y_offset - (double)short_range) / log(steepness);
}

static double long_range_to_mm(uint16_t long_range)
{
    double shift = 250;
    double steepness = 1.0375;
    double y_offset = 3450;

    return shift - log(y_offset - (double)long_range) / log(steepness);
}

static double combined_to_mm(uint16_t short_range, uint16_t long_range)
{
    const double long_range_start = 34;
    const double short_range_end = 49;

    double distance_short_range = short_range_to_mm(short_range);
    double distance_long_range = long_range_to_mm(long_range);

    if (distance_short_range < long_range_start)
        return distance_short_range;

    if (distance_long_range > short_range_end)
        return distance_long_range;

    double ratio = (1 / (short_range_end - long_range_start)) * (distance_long_range - long_range_start);

    return (1 - ratio) * distance_short_range + ratio * distance_long_range;
}

int ir_read(struct ir_distances* ret_distances)
{
    ret_distances->capture_cycle = capture_cycle;

    if (capture_cycle < 10) {
        return -1;
    }

    for (enum ir_sensor sensor = IR_LEFT; sensor < IR_SENSOR_COUNT; sensor++) {
        ret_distances->sensor_mm[sensor] = combined_to_mm(
            measurements[sensor].short_range, measurements[sensor].long_range);
    }

    return 0;
}

static void cmd_print_all(shell_compat_process_t* process, int argc, char** argv)
{
    struct ir_distances distances;
    ir_tick();
    if (ir_read(&distances) < 0) {
        return;
    }

    printf("* left:  %6.2f, front left: %6.2f, front: %6.2f, front right: %6.2f, right: %6.2f\n",
        distances.sensor_mm[IR_LEFT],
        distances.sensor_mm[IR_FRONT_LEFT],
        distances.sensor_mm[IR_FRONT],
        distances.sensor_mm[IR_FRONT_RIGHT],
        distances.sensor_mm[IR_RIGHT]);
}

static void cmd_print_all_raw(shell_compat_process_t* process, int argc, char** argv)
{
    ir_tick();

    printf("* ");
    for (enum ir_sensor sensor = IR_LEFT; sensor < IR_SENSOR_COUNT; sensor++) {
        char* name
            = sensor == IR_LEFT        ? "left"
            : sensor == IR_FRONT_LEFT  ? "front left"
            : sensor == IR_FRONT       ? "front"
            : sensor == IR_FRONT_RIGHT ? "front right"
            : sensor == IR_RIGHT       ? "right"
                                       : "<invalid>";

        printf("%s: %4d, %4d, ", name,
            measurements[sensor].short_range, measurements[sensor].long_range);
    }
    printf("\n");
}

static void cmd_print_raw(shell_compat_process_t* process, int argc, char** argv)
{
    if (shell_compat_iteration(process) == 0) {
        start_capturing();
        if (argc >= 2) {
            select_sensor(strtol(argv[1], NULL, 0));
        }
    }

    if (dma_channel_is_busy(dma_chan)) {
        return;
    }

    uint16_t valleys[PERIOD_COUNT];
    for (int period = 0; period < PERIOD_COUNT; period++) {
        valleys[period] = UINT16_MAX;

        for (int sample = 0; sample < PERIOD_SAMPLES; sample++) {
            int offset = (period * PERIOD_SAMPLES) + sample;

            if (capture_buffer[offset] < valleys[period]) {
                valleys[period] = capture_buffer[offset];
            }
        }
    }

    for (int period = 0; period < PERIOD_COUNT; period++) {
        for (int sample = 0; sample < PERIOD_SAMPLES; sample++) {
            int offset = (period * PERIOD_SAMPLES) + sample;

            printf("* %4d, %4d\n", capture_buffer[offset], valleys[period]);
        }
    }

    stop_capturing();

    shell_compat_exit(process);
}

void ir_init()
{
    memset(measurements, 0, sizeof(measurements));

    // Use pwm channel to get a 10kHz clock
    unsigned int tx_pin = PIN_IR_TX_CLK;

    unsigned int slice_num = pwm_gpio_to_slice_num(tx_pin);
    unsigned int channel = pwm_gpio_to_channel(tx_pin);

    pwm_set_clkdiv(slice_num, 10);
    pwm_set_wrap(slice_num, 1250); // System clock speed is 125 MHz
    pwm_set_chan_level(slice_num, channel, 1250 / 2);
    pwm_set_enabled(slice_num, true);

    gpio_set_function(tx_pin, GPIO_FUNC_PWM);

    adc_init();
    adc_gpio_init(PIN_IR_RX1);
    adc_gpio_init(PIN_IR_RX2);

    select_sensor(IR_LEFT);

    adc_fifo_setup(true, true, 1, true, false);
    adc_set_clkdiv(0);

    dma_chan = dma_claim_unused_channel(true);

    adc_run(true);

    shell_compat_register("ir-print-all", "Print IR sensor data", cmd_print_all);
    shell_compat_register("ir-print-all-raw", "Print IR sensor data", cmd_print_all_raw);
    shell_compat_register("ir-print-raw", "Print raw IR sensor data", cmd_print_raw);
}
