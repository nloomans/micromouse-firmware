#pragma once
#include <pico/time.h>
#include <stdint.h>

void motor_init();

enum motor { // Defined types to call motor_drive with left or right motor
    MOTOR_LEFT,
    MOTOR_RIGHT
};

#define MOTOR_COUNT 2

enum motor_drive { // Defined types to call motor_drive with driving behavior
    MOTOR_FORWARD,
    MOTOR_BACKWARD,
    MOTOR_BREAK
};

int motor_distance(enum motor motor);

void motor_set_duty(enum motor motor, enum motor_drive motor_drive, uint16_t duty);

void motor_straight(uint16_t duty);

void motor_turn(enum motor turn_dir, uint16_t duty);

void motor_off();

void motor_maintain_speed(enum motor motor, double speed_ref);
