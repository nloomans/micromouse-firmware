#pragma once
#include <stdbool.h>
#include <stdint.h>

void shift_init();

void shift_put(uint32_t spin, bool value);

void shift_flush();
