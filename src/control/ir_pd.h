#pragma once
#include "../drivers/ir.h"
#include <pico/time.h>

struct ir_pd_state {
    double calibration_gain;
    double prev_ir_left;
    double prev_ir_right;
    absolute_time_t last_sample_timestamp;
    uint64_t last_cycle;
    double prev_retval;
};

void ir_pid_init(struct ir_pd_state* state, struct ir_distances distances);

double ir_pid_get_angle_offset(struct ir_pd_state* state, struct ir_distances distances);
