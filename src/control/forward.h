#pragma once
#include "../drivers/ir.h"
#include "../drivers/motor.h"
#include <pico/types.h>
#include <stdbool.h>

enum forward_goal {
    FORWARD_GOAL_NONE,
    FORWARD_GOAL_GAP_START_LEFT,
    FORWARD_GOAL_GAP_START_RIGHT,
    FORWARD_GOAL_GAP_MIDDLE,
};

enum forward_stop_cond {
    FORWARD_STOP_COND_WALL_FRONT = 1 << 0,
    FORWARD_STOP_COND_GAP_LEFT = 1 << 1,
    FORWARD_STOP_COND_GAP_RIGHT = 1 << 2,
};

struct forward_result {
    int units_driven;
};

struct forward_state {
    enum forward_stop_cond stop_cond;
    struct ir_pd_state* ir_pid_state;
    enum forward_goal goal;
    double angle_ref;
    double prev_error;
    double integral;
    absolute_time_t prev_time;

    bool left_ready;
    bool right_ready;

    int initial_distance[MOTOR_COUNT];

    // FORWARD_GOAL_GAP_MIDDLE
    int distance_goal[MOTOR_COUNT];
};

void forward_init(struct forward_state* state,
    struct ir_pd_state* ir_pid_state,
    enum forward_stop_cond stop_cond,
    double angle_ref);
bool forward_tick(struct forward_result* ret, struct forward_state* state, struct ir_distances distances);
