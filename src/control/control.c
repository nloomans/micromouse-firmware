#include "control.h"
#include "../drivers/gyro.h"
#include "../drivers/ir.h"
#include "../drivers/motor.h"
#include "../drivers/pins.h"
#include "../drivers/shift.h"
#include "../lib/ipc.h"
#include "../maze.h"
#include "../shell_compat.h"
#include "../sync/follow_wall.h"
#include "../sync/solve_maze.h"
#include "forward.h"
#include "hardware/timer.h"
#include "ir_pd.h"
#include "pico/time.h"
#include "pico/types.h"
#include "rotate.h"
#include <string.h>

static struct maze maze;

struct state {
    const struct ipc_request* request;
    int64_t iteration;
    struct ir_pd_state ir_pd_state;

    // IPC_REQUEST_ROTATE
    struct rotate_state rotate_state;

    // IPC_REQUEST_FORWARD
    struct forward_state forward_state;

    // IPC_OP_STEP_BACK
    absolute_time_t step_back_timeout;
};

static void init(struct state* follow_wall)
{
    struct ir_distances distances;
    while (true) {
        ir_tick();
        if (ir_read(&distances) < 0) {
            continue;
        }

        if (distances.sensor_mm[IR_FRONT] < 100) {
            shift_put(SPIN_LED_3, true);
            shift_flush();
            break;
        }
    }

    while (true) {
        ir_tick();
        if (ir_read(&distances) < 0) {
            continue;
        }

        if (distances.sensor_mm[IR_FRONT] >= 100) {
            shift_put(SPIN_LED_3, false);
            shift_flush();
            break;
        }
    }

    gyro_calibrate();
    gyro_start();

    gyro_tick();
    ir_tick();

    *follow_wall = (struct state) { 0 };

    ir_pid_init(&follow_wall->ir_pd_state, distances);
}

bool tick(struct state* state)
{
    gyro_tick();
    ir_tick();

    struct ir_distances distances;
    if (ir_read(&distances) < 0) {
        return false;
    }

    if (state->request == NULL) {
        state->request = ipc_recv();
        state->iteration = -1;
    }

    if (state->request == NULL) {
        return false;
    }

    state->iteration++;
    bool once = state->iteration == 0;

    switch (state->request->op) {
    case IPC_OP_PING: {
        union ipc_response response = {
            .ping = state->request->data.ping,
        };
        ipc_reply(&state->request, &response);
        break;
    }

    case IPC_OP_GET_IR: {
        union ipc_response response = {
            .get_ir = distances,
        };
        ipc_reply(&state->request, &response);
        break;
    }

    case IPC_OP_GET_GYRO: {
        union ipc_response response = {
            .get_gyro = gyro_rotation(),
        };
        ipc_reply(&state->request, &response);
        break;
    }

    case IPC_OP_ROTATE_TO: {
        if (once) {
            rotate_init(&state->rotate_state, state->request->data.rotate_to);
        }

        if (rotate_tick(&state->rotate_state)) {
            union ipc_response response = {
                .rotate_to = IPC_VOID,
            };
            ipc_reply(&state->request, &response);
        }

        break;
    }

    case IPC_OP_FORWARD: {
        if (once) {
            forward_init(&state->forward_state,
                &state->ir_pd_state,
                state->request->data.forward.stop_cond,
                state->request->data.forward.angle);
        }

        struct forward_result result = { 0 };
        if (forward_tick(&result, &state->forward_state, distances)) {
            union ipc_response response = {
                .forward = result,
            };
            ipc_reply(&state->request, &response);
        }

        break;
    }

    case IPC_OP_STEP_BACK: {
        if (once) {
            state->step_back_timeout = make_timeout_time_ms(500);
        }

        if (time_reached(state->step_back_timeout)) {
            motor_off();
            union ipc_response response = {
                .step_back = IPC_VOID,
            };
            ipc_reply(&state->request, &response);
        } else {
            motor_set_duty(MOTOR_LEFT, MOTOR_BACKWARD, 0x6000);
            motor_set_duty(MOTOR_RIGHT, MOTOR_BACKWARD, 0x6000);
        }
        break;
    }

    case IPC_OP_SAVE_MAZE: {
        memcpy(&maze, state->request->data.save_maze, sizeof(struct maze));
        union ipc_response response = {
            .save_maze = IPC_VOID,
        };
        ipc_reply(&state->request, &response);
        break;
    }

    case IPC_OP_LOAD_MAZE: {
        memcpy(state->request->data.load_maze, &maze, sizeof(struct maze));
        union ipc_response response = {
            .save_maze = IPC_VOID,
        };
        ipc_reply(&state->request, &response);
        break;
    }
    }

    return false;
}

static void cleanup()
{
    motor_off();
    ipc_stop();
}

static void cmd_follow_wall(shell_compat_process_t* process, int argc, char** argv)
{
    static struct state state;
    if (shell_compat_iteration(process) == 0) {
        shell_compat_set_cleanup_callback(process, &cleanup);
        memset(&state, 0, sizeof(state));
        init(&state);
        ipc_start(follow_wall);
    }

    if (tick(&state)) {
        shell_compat_exit(process);
    }

    if (button_read() & BUTTON_1) {
        shell_compat_exit(process);
    }
}

static void cmd_solve_maze(shell_compat_process_t* process, int argc, char** argv)
{
    static struct state state;
    if (shell_compat_iteration(process) == 0) {
        shell_compat_set_cleanup_callback(process, &cleanup);
        memset(&state, 0, sizeof(state));
        init(&state);
        ipc_start(solve_maze);
    }

    if (tick(&state)) {
        shell_compat_exit(process);
    }

    if (button_read() & BUTTON_1) {
        shell_compat_exit(process);
    }
}

void control_init()
{
    maze_init(&maze);

    shell_compat_register("follow-wall", "Run the wall follower", cmd_follow_wall);
    shell_compat_register("solve-maze", "Run the maze solver", cmd_solve_maze);
    shell_register_shortcut(BUTTON_3, "follow-wall");
    shell_register_shortcut(BUTTON_4, "solve-maze");
}
