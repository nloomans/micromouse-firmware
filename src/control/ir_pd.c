#include "ir_pd.h"
#include "../drivers/gyro.h"
#include "../drivers/ir.h"
#include "../log.h"
#include <math.h>
#include <pico/time.h>

#define TAG "ir_pd"

const double maze_width = 130; // mm
const double mouse_width = 59; // mm
const double wall_threshold = 80; // Any distance above this is considered a hole, unit in mm
const double ir_differential_difference_threshold = 0.1; // m/s

struct pd_data {
    double diff;
    double prop;
};

struct pd_data calculate_error(struct ir_pd_state* state, double ir_left, double ir_right)
{
    ir_left *= state->calibration_gain;
    ir_right *= state->calibration_gain;
    if (isinf(ir_left))
        ir_left = 1000;
    if (isinf(ir_right))
        ir_right = 1000;

    absolute_time_t current_time = get_absolute_time();
    double time_diff_us = absolute_time_diff_us(state->last_sample_timestamp, current_time);

    double ir_left_diff = (ir_left - state->prev_ir_left) / time_diff_us * 1000; // m/s
    double ir_right_diff = (ir_right - state->prev_ir_right) / time_diff_us * 1000;

    state->prev_ir_left = ir_left;
    state->prev_ir_right = ir_right;
    state->last_sample_timestamp = current_time;

    struct pd_data data = {
        .diff = NAN,
        .prop = NAN,
    };

    if (fabs(ir_left_diff + ir_right_diff) < ir_differential_difference_threshold) {
        data.diff = (ir_left_diff - ir_right_diff) / 2;
    }

    if (ir_left < wall_threshold && ir_right < wall_threshold) {
        data.prop = (ir_left - ir_right) / 2;
    } else if (ir_left > wall_threshold && ir_right < wall_threshold) {
        data.prop = maze_width / 2 - (ir_right + mouse_width / 2);
    } else if (ir_left < wall_threshold && ir_right > wall_threshold) {
        data.prop = (ir_left + mouse_width / 2) - maze_width / 2;
    }

    return data;
}

void ir_pid_init(struct ir_pd_state* state, struct ir_distances distances)
{
    double ir_left = distances.sensor_mm[IR_LEFT];
    double ir_right = distances.sensor_mm[IR_RIGHT];

    state->calibration_gain = (ir_left + ir_right + mouse_width) / maze_width;
    log_debugf(TAG, "calibration gain: %lf", state->calibration_gain);

    ir_left *= state->calibration_gain;
    ir_right *= state->calibration_gain;

    log_debugf(TAG, "ir_left: %lf, ir_right: %lf", ir_left, ir_right);

    state->prev_ir_left = ir_left;
    state->prev_ir_right = ir_right;
    state->last_sample_timestamp = get_absolute_time();
    state->last_cycle = distances.capture_cycle;
    state->prev_retval = 0;
}

double ir_pid_get_angle_offset(struct ir_pd_state* state, struct ir_distances distances)
{
    if (distances.capture_cycle == state->last_cycle) {
        return state->prev_retval;
    }

    struct pd_data data = calculate_error(state, distances.sensor_mm[IR_LEFT], distances.sensor_mm[IR_RIGHT]);

    const double kp = -0.1;
    const double kd = -50;

    double angle_pd;
    double proportional = kp * data.prop;
    double differential = kd * data.diff;

    if (isnan(proportional) && isnan(differential))
        angle_pd = 0;
    else if (isnan(proportional)) {
        angle_pd = differential;
    } else if (isnan(differential)) {
        angle_pd = proportional;
    } else {
        angle_pd = proportional + differential;
    }

    if (angle_pd > 3)
        angle_pd = 3;
    else if (angle_pd < -3)
        angle_pd = -3;

    state->last_cycle = distances.capture_cycle;
    state->prev_retval = angle_pd;

    return angle_pd;
}
