#pragma once
#include <pico/types.h>
#include <stdbool.h>

struct rotate_state {
    double angle_ref;
    double prev_error;
    double integral;
    absolute_time_t prev_time;
    absolute_time_t timeout;
    uint64_t iteration;
    float history[10];
};

void rotate_init(struct rotate_state* state, double angle_ref);
bool rotate_tick(struct rotate_state* state);
