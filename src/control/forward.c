#include "forward.h"
#include "../drivers/gyro.h"
#include "../drivers/ir.h"
#include "../drivers/motor.h"
#include "../drivers/pins.h"
#include "../drivers/shift.h"
#include "../log.h"
#include "ir_pd.h"
#include <math.h>
#include <pico/time.h>

#define TAG "forward"

#define KP 350.0
#define KI 0
#define KD 2500000.0

#define MINIMUM 13000

#define MOTOR_DISTANCE 20

#define UNIT_DISTANCE 54

void forward_init(struct forward_state* state,
    struct ir_pd_state* ir_pid_state,
    enum forward_stop_cond stop_cond,
    double angle_ref)
{
    *state = (struct forward_state) {
        .stop_cond = stop_cond,
        .goal = FORWARD_GOAL_NONE,
        .angle_ref = angle_ref,
        .ir_pid_state = ir_pid_state,
        .prev_time = get_absolute_time(),
        .initial_distance = { motor_distance(MOTOR_LEFT), motor_distance(MOTOR_RIGHT) },
        .left_ready = false,
        .right_ready = false,
    };
}

static void forward_pid(struct forward_state* state, struct ir_distances distances, double mult_l, double mult_r)
{
    double angle_offset = ir_pid_get_angle_offset(state->ir_pid_state, distances);

    double angle_error
        = (state->angle_ref - angle_offset) - gyro_rotation();
    if (angle_error == state->prev_error)
        return;

    absolute_time_t curr_time = get_absolute_time();

    double proportional = KP * angle_error;
    state->integral += KI * angle_error * (double)absolute_time_diff_us(state->prev_time, curr_time);
    double differential = KD * (angle_error - state->prev_error)
        / (double)absolute_time_diff_us(state->prev_time, curr_time);

    double speed = (proportional + differential + state->integral) * 0.1;

    double speed_l = (500 - speed) * mult_l;
    double speed_r = (500 + speed) * mult_r;

    // log_debugf_slow(10, TAG, "speed: %5.2f, %5.2f, motor_distance: %5d, %5d",
    //     speed_l, speed_r, motor_distance(MOTOR_LEFT), motor_distance(MOTOR_RIGHT));

    motor_maintain_speed(MOTOR_LEFT, speed_l);
    motor_maintain_speed(MOTOR_RIGHT, speed_r);
}

int calc_units_driven(struct forward_state* state)
{
    double distance_left = motor_distance(MOTOR_LEFT) - state->initial_distance[MOTOR_LEFT];
    double distance_right = motor_distance(MOTOR_RIGHT) - state->initial_distance[MOTOR_RIGHT];
    double distance = distance_left + distance_right / 2;
    return round(distance / UNIT_DISTANCE);
}

bool forward_tick(struct forward_result* ret_result, struct forward_state* state, struct ir_distances distances)
{
    if (state->stop_cond & FORWARD_STOP_COND_WALL_FRONT && distances.sensor_mm[IR_FRONT] <= 20) {
        motor_off();
        *ret_result = (struct forward_result) {
            .units_driven = calc_units_driven(state),
        };
        return true;
    }

    if (state->stop_cond & FORWARD_STOP_COND_WALL_FRONT && distances.sensor_mm[IR_FRONT] <= 80) {
        double error = (distances.sensor_mm[IR_FRONT] - 20) / 60;
        forward_pid(state, distances, 0.3 + error * 0.7, 0.3 + error * 0.7);

    } else if (state->goal == FORWARD_GOAL_GAP_MIDDLE) {
        double error[MOTOR_COUNT];
        for (enum motor motor = 0; motor < MOTOR_COUNT; motor++) {
            error[motor] = (double)(state->distance_goal[motor] - motor_distance(motor)) / MOTOR_DISTANCE;
        }

        if (error[MOTOR_LEFT] <= 0.1 || error[MOTOR_RIGHT] <= 0.1) {
            motor_off();
            *ret_result = (struct forward_result) {
                .units_driven = calc_units_driven(state),
            };
            return true;
        } else {
            forward_pid(state, distances,
                0.3 + error[MOTOR_LEFT] * 0.7,
                0.3 + error[MOTOR_RIGHT] * 0.7);
        }
    } else {
        forward_pid(state, distances, 1, 1);
    }

    if (distances.sensor_mm[IR_LEFT] <= 100 && !state->left_ready) {
        state->left_ready = true;
    }

    if (distances.sensor_mm[IR_RIGHT] <= 100 && !state->right_ready) {
        state->right_ready = true;
    }

    if (state->goal == FORWARD_GOAL_NONE) {
        if (((state->stop_cond & FORWARD_GOAL_GAP_START_LEFT) && state->left_ready && distances.sensor_mm[IR_LEFT] > 100)
            || ((state->stop_cond & FORWARD_GOAL_GAP_START_RIGHT) && state->right_ready && distances.sensor_mm[IR_RIGHT] > 100)) {

            state->goal = FORWARD_GOAL_GAP_MIDDLE;
            state->distance_goal[MOTOR_LEFT] = motor_distance(MOTOR_LEFT) + MOTOR_DISTANCE;
            state->distance_goal[MOTOR_RIGHT] = motor_distance(MOTOR_RIGHT) + MOTOR_DISTANCE;
        }
    }

    return false;
}
