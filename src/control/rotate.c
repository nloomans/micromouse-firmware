#include "rotate.h"
#include "../drivers/gyro.h"
#include "../drivers/motor.h"
#include "../lib/macro.h"
#include "hardware/timer.h"
#include "pico/types.h"
#include <math.h>
#include <pico/time.h>

#define MINIMUM 13000
#define KI 0.002
#define KP 350.0
#define KD 2500000.0

void rotate_init(struct rotate_state* state, double angle_ref)
{
    *state = (struct rotate_state) {
        .angle_ref = angle_ref,
        .prev_time = get_absolute_time(),
        .timeout = make_timeout_time_ms(2000),
        .iteration = 0,
        .history = {
            -INFINITY,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            INFINITY,
        }

    };
}

bool is_idle(struct rotate_state* state)
{
    float lowest = INFINITY, highest = -INFINITY;
    for (size_t i = 0; i < ARRAY_SIZE(state->history); i++) {
        if (state->history[i] < lowest)
            lowest = state->history[i];
        if (state->history[i] > highest)
            highest = state->history[i];
    }
    float difference = lowest - highest;
    if (difference < 0)
        difference = -difference;

    return difference < 0.01;
}

bool rotate_tick(struct rotate_state* state)
{
    state->iteration++;

    state->history[state->iteration % ARRAY_SIZE(state->history)]
        = gyro_rotation();

    double angle_error = state->angle_ref - gyro_rotation();
    if (angle_error == state->prev_error)
        return false;

    // hacky more consistent rotate
    if (angle_error < -30) {
        motor_maintain_speed(MOTOR_LEFT, 500);
        motor_maintain_speed(MOTOR_RIGHT, -500);
        return false;
    } else if (angle_error > 30) {
        motor_maintain_speed(MOTOR_LEFT, -500);
        motor_maintain_speed(MOTOR_RIGHT, 500);
        return false;
    }

    absolute_time_t curr_time = get_absolute_time();

    if (angle_error > -15 && angle_error < 15) {
        state->integral += KI * angle_error * (double)absolute_time_diff_us(state->prev_time, curr_time);
    } else {
        state->integral = 0;
    }

    double proportional = KP * angle_error;
    double differential = KD * (angle_error - state->prev_error)
        / (double)absolute_time_diff_us(state->prev_time, curr_time);

    int32_t duty = proportional + differential + state->integral;

    // log_printf("error: %5.2f, p: %10.2f, i: %10.2f, d: %10.2f, duty: %4d, d: %llu µs\n", angle_error, proportional, state->integral, differential, duty, absolute_time_diff_us(state->prev_time, curr_time));

    if (time_reached(state->timeout) || (angle_error > -2 && angle_error < 2 && is_idle(state))) {
        return true;
    }

    if (duty > -120 && duty < 120) {
        motor_off(); // Deadspot to decrease oscilations.
    } else {
        if (duty < 0) { // Negative angle difference, turn the other way
            duty = -duty;
            duty = duty + MINIMUM;
            duty = MIN(duty, 0xffff);

            motor_turn(MOTOR_RIGHT, duty);
        } else {
            duty = duty + MINIMUM;
            duty = MIN(duty, 0xffff);

            motor_turn(MOTOR_LEFT, duty);
        }
    }

    state->prev_time = curr_time;
    state->prev_error = angle_error;

    return false;
}
