#include "interrupt.h"
#include "drivers/pins.h"
#include "hardware/gpio.h"
#include "hardware/sync.h"

static struct interrupt_gpio_registration* gpio_registrations[PIN_MAX + 1] = { 0 };

void _interrupt_gpio_register(struct interrupt_gpio_registration* registration)
{
    assert(registration->gpio <= PIN_MAX);

    uint32_t saved_interrupts = save_and_disable_interrupts();
    registration->next = gpio_registrations[registration->gpio];
    gpio_registrations[registration->gpio] = registration;
    restore_interrupts(saved_interrupts);

    gpio_set_irq_enabled(registration->gpio, registration->event_mask, true);
}

static void global_gpio_irq_handler(unsigned int gpio, uint32_t event_mask)
{
    struct interrupt_gpio_registration* registration = gpio_registrations[gpio];

    while (registration != NULL) {
        if ((event_mask & registration->event_mask) != 0) {
            registration->handler(gpio, event_mask);
        }

        registration = registration->next;
    }
}

void interrupt_init()
{
    gpio_set_irq_callback(global_gpio_irq_handler);

    // Unsure what this does, but it is needed for irq to work.
    // gpio_set_irq_enabled_with_callback() calls this internally.
    irq_set_enabled(IO_IRQ_BANK0, true);
}
