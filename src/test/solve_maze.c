#include "../sync/solve_maze.h"
#include "../lib/ipc.h"
#include "../lib/macro.h"
#include "../log.h"
#include "../maze.h"
#include "../shell_compat.h"
#include "../sync/follow_wall.h"
#include <math.h>
#include <pico/multicore.h>
#include <pico/rand.h>
#include <stdint.h>
#include <string.h>

static struct maze maze;
static struct maze solver_maze;

struct state {
    const struct ipc_request* request;
    int64_t iteration;

    struct vector pos;
    double angle;
    enum car_dir car_dir;
};

static void shuffle(enum car_dir* array, size_t n)
{
    if (n > 1) {
        size_t i;
        for (i = 0; i < n - 1; i++) {
            size_t j = i + get_rand_32() / (UINT32_MAX / (n - i) + 1);
            enum car_dir t = array[j];
            array[j] = array[i];
            array[i] = t;
        }
    }
}

static void fill_randomly(struct maze* maze, struct vector starting_pos, int layer)
{
    enum car_dir dirs[] = { CAR_DIR_NORTH, CAR_DIR_EAST, CAR_DIR_SOUTH, CAR_DIR_WEST };
    shuffle(dirs, 4);

    for (int i = 0; i < 4; i++) {
        enum car_dir dir = dirs[i];
        struct vector next_pos = maze_square_get_neighbour_pos(starting_pos, dir);
        if (next_pos.y < 0 || next_pos.y >= MAZE_FIELDS_WIDTH
            || next_pos.x < 0 || next_pos.x >= MAZE_FIELDS_WIDTH) {
            continue;
        }
        if (maze_get_field(maze, next_pos) != MAZE_FIELD_UNKNOWN) {
            continue;
        }

        maze_set_wall(maze, starting_pos, dir, MAZE_FIELD_EMPTY);
        maze_set_square(maze, next_pos, MAZE_FIELD_EMPTY);

        fill_randomly(maze, next_pos, layer + 1);
    }
}

static void init(struct state* state)
{
    memset(state, 0, sizeof(*state));

    state->pos = MAZE_STARTING_SQUARE_BOTTOM_LEFT;
}

static bool tick(struct state* state)
{
    if (state->request == NULL) {
        state->request = ipc_recv();
        state->iteration = -1;
    }

    if (state->request == NULL) {
        return false;
    }

    state->iteration++;

    switch (state->request->op) {
    case IPC_OP_PING: {
        union ipc_response response = {
            .ping = state->request->data.ping,
        };
        ipc_reply(&state->request, &response);
        break;
    }

    case IPC_OP_GET_IR: {
        union ipc_response response = {
            .get_ir = {
                .sensor_mm = {
                    [IR_LEFT] = maze_get_wall(&maze, state->pos, car_dir_rotate(state->car_dir, 1)) == MAZE_FIELD_WALL ? 30 : 120,
                    [IR_FRONT_LEFT] = INFINITY,
                    [IR_FRONT] = maze_get_wall(&maze, state->pos, state->car_dir) == MAZE_FIELD_WALL ? 30 : 120,
                    [IR_FRONT_RIGHT] = INFINITY,
                    [IR_RIGHT] = maze_get_wall(&maze, state->pos, car_dir_rotate(state->car_dir, -1)) == MAZE_FIELD_WALL ? 30 : 120,
                },
            },
        };
        ipc_reply(&state->request, &response);
        break;
    }

    case IPC_OP_GET_GYRO: {
        union ipc_response response = {
            .get_gyro = state->angle,
        };
        ipc_reply(&state->request, &response);
        break;
    }

    case IPC_OP_ROTATE_TO: {
        double diff = state->request->data.rotate_to - state->angle;
        int rotation = diff / 90;
        // printf("diff: %f, rotation: %d\n", diff, rotation);

        state->angle = state->request->data.rotate_to;
        state->car_dir = car_dir_rotate(state->car_dir, rotation);
        // printf("angle: %f, car_dir: %d\n", state->angle, state->car_dir);

        union ipc_response response = {
            .rotate_to = IPC_VOID,
        };
        ipc_reply(&state->request, &response);

        break;
    }

    case IPC_OP_FORWARD: {
        // printf("before: y: %d, x: %d\n", state->pos.y, state->pos.x);
        int units_driven = 0;
        if (
            maze_get_wall(&maze, state->pos, state->car_dir) != MAZE_FIELD_WALL) {
            state->pos = maze_square_get_neighbour_pos(state->pos, state->car_dir);
            units_driven++;

            // printf("y: %d, x: %d\n", state->pos.y, state->pos.x);
        }

        while (
            maze_get_wall(&maze, state->pos, state->car_dir) != MAZE_FIELD_WALL
            && maze_get_wall(&maze, state->pos, car_dir_rotate(state->car_dir, 1)) == MAZE_FIELD_WALL
            && maze_get_wall(&maze, state->pos, car_dir_rotate(state->car_dir, -1)) == MAZE_FIELD_WALL) {
            state->pos = maze_square_get_neighbour_pos(state->pos, state->car_dir);
            units_driven++;

            // printf("y: %d, x: %d\n", state->pos.y, state->pos.x);
        }

        union ipc_response response = {
            .forward = {
                .units_driven = units_driven,
            },
        };
        ipc_reply(&state->request, &response);

        break;
    }

    case IPC_OP_STEP_BACK: {
        union ipc_response response = {
            .step_back = IPC_VOID,
        };
        ipc_reply(&state->request, &response);
        break;
    }

    case IPC_OP_SAVE_MAZE: {
        memcpy(&solver_maze, state->request->data.save_maze, sizeof(struct maze));
        union ipc_response response = {
            .save_maze = IPC_VOID,
        };
        ipc_reply(&state->request, &response);
        break;
    }

    case IPC_OP_LOAD_MAZE: {
        maze_print(&solver_maze);
        memcpy(state->request->data.load_maze, &solver_maze, sizeof(struct maze));
        union ipc_response response = {
            .save_maze = IPC_VOID,
        };
        ipc_reply(&state->request, &response);
        break;
    }
    }

    return false;
}

static void cmd_solve_maze_test(shell_compat_process_t* process, int argc, char** argv)
{
    static struct state state;

    if (shell_compat_iteration(process) == 0) {
        shell_compat_set_cleanup_callback(process, &ipc_stop);
        memset(&state, 0, sizeof(state));
        init(&state);
        maze_print(&maze);

        ipc_start(solve_maze);
    }

    if (tick(&state)) {
        shell_compat_exit(process);
    }
}

void solve_maze_test_init()
{
    maze_init(&solver_maze);

    maze_init(&maze);
    for (int y = 1; y < MAZE_FIELDS_WIDTH; y += 2) {
        for (int x = 0; x < MAZE_FIELDS_WIDTH; x += 2) {
            maze.fields[y][x] = MAZE_FIELD_WALL;
        }
    }
    for (int y = 0; y < MAZE_FIELDS_WIDTH; y += 2) {
        for (int x = 1; x < MAZE_FIELDS_WIDTH; x += 2) {
            maze.fields[y][x] = MAZE_FIELD_WALL;
        }
    }

    struct vector goals[] = CONFIG_MAZE_GOALS;

    bool created_opening = false;

    for (size_t i = 0; i < ARRAY_SIZE(goals); i++) {
        maze_set_square(&maze, goals[i], MAZE_FIELD_EMPTY);

        for (enum car_dir car_dir = CAR_DIR_MAX; car_dir < 4; car_dir++) {
            struct vector neighbour = maze_square_get_neighbour_pos(goals[i], car_dir);

            if (!maze_is_within_bounds(neighbour)) {
                continue;
            }

            bool has_adjacent_goal = false;
            for (size_t j = 0; j < ARRAY_SIZE(goals); j++) {
                if (vector_equal(neighbour, goals[j])) {
                    maze_set_wall(&maze, goals[i], car_dir, MAZE_FIELD_EMPTY);
                    has_adjacent_goal = true;
                }
            }

            if (!has_adjacent_goal && !created_opening) {
                maze_set_wall(&maze, goals[i], car_dir, MAZE_FIELD_EMPTY);
                created_opening = true;
            }
        }
    }

    maze_set_field(&maze, MAZE_STARTING_SQUARE_BOTTOM_LEFT, MAZE_FIELD_EMPTY);
    maze_set_wall(&maze, MAZE_STARTING_SQUARE_BOTTOM_LEFT, CAR_DIR_NORTH, MAZE_FIELD_EMPTY);

    fill_randomly(&maze, goals[0], 0);

    shell_compat_register("solve-maze-test", "Run the in-memory maze solver test", cmd_solve_maze_test);
}
