#include "robot.h"
#include "../control/forward.h"
#include "../drivers/ir.h"
#include "../lib/ipc.h"
#include "../log.h"
#include <pico/assert.h>

#define TAG "robot"

struct ir_distances robot_get_ir()
{
    const struct ipc_request request = {
        .op = IPC_OP_GET_IR,
    };

    union ipc_response response;
    log_infof(TAG, "get_ir()");
    ipc_send(&response, &request);
    return response.get_ir;
}

double robot_get_gyro()
{
    const struct ipc_request request = {
        .op = IPC_OP_GET_GYRO,
    };

    union ipc_response response;
    log_infof(TAG, "get_gyro()");
    ipc_send(&response, &request);
    return response.get_gyro;
}

void robot_rotate_to(double angle)
{
    const struct ipc_request request = {
        .op = IPC_OP_ROTATE_TO,
        .data = {
            .rotate_to = angle,
        },
    };

    union ipc_response response;
    log_infof(TAG, "rotate_to(%.2f)", angle);
    ipc_send(&response, &request);
    hard_assert(response.rotate_to == IPC_VOID);
}

struct forward_result robot_forward(double angle, enum forward_stop_cond stop_cond)
{
    const struct ipc_request request = {
        .op = IPC_OP_FORWARD,
        .data = {
            .forward = {
                .angle = angle,
                .stop_cond = stop_cond,
            },
        },
    };

    union ipc_response response;
    log_infof(TAG, "forward(%.2f, %d)", angle, stop_cond);
    ipc_send(&response, &request);
    return response.forward;
}

void robot_step_back()
{
    const struct ipc_request request = {
        .op = IPC_OP_STEP_BACK,
        .data = {
            .step_back = IPC_VOID,
        },
    };

    union ipc_response response;
    log_infof(TAG, "step_back()");
    ipc_send(&response, &request);
    hard_assert(response.step_back == IPC_VOID);
}

void robot_save_maze(const struct maze* maze)
{
    const struct ipc_request request = {
        .op = IPC_OP_SAVE_MAZE,
        .data = {
            .save_maze = maze,
        },
    };

    union ipc_response response;
    log_infof(TAG, "save_made(%p)", (void*)maze);
    ipc_send(&response, &request);
    hard_assert(response.step_back == IPC_VOID);
}

void robot_load_maze(struct maze* maze)
{
    const struct ipc_request request = {
        .op = IPC_OP_LOAD_MAZE,
        .data = {
            .load_maze = maze,
        },
    };

    union ipc_response response;
    log_infof(TAG, "load_made(%p)", (void*)maze);
    ipc_send(&response, &request);
    hard_assert(response.step_back == IPC_VOID);
}
