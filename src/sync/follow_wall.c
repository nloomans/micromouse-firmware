#include "../control/forward.h"
#include "../drivers/ir.h"
#include "../log.h"
#include "pico/time.h"
#include "robot.h"
#include <pico/assert.h>

#define TAG "core1"

double calibrate_angle_against_wall(void)
{
    robot_step_back();
    sleep_ms(200);
    return robot_get_gyro();
}

void follow_wall(void)
{
    double angle = calibrate_angle_against_wall();

    while (true) {
        struct ir_distances distances = robot_get_ir();
        log_debugf(TAG, "left: %5.2f, front: %5.2f, right: %5.2f", distances.sensor_mm[IR_LEFT], distances.sensor_mm[IR_FRONT], distances.sensor_mm[IR_RIGHT]);

        if (distances.sensor_mm[IR_LEFT] >= 100) {
            angle -= 90;
            robot_rotate_to(angle);
            robot_forward(angle,
                FORWARD_STOP_COND_WALL_FRONT | FORWARD_STOP_COND_GAP_RIGHT);
        }

        else if (distances.sensor_mm[IR_FRONT] >= 100) {
            robot_forward(angle,
                FORWARD_STOP_COND_WALL_FRONT | FORWARD_STOP_COND_GAP_RIGHT);
        }

        else if (distances.sensor_mm[IR_FRONT] >= 100) {
            angle += 90;
            robot_rotate_to(angle);
            robot_forward(angle,
                FORWARD_STOP_COND_WALL_FRONT | FORWARD_STOP_COND_GAP_RIGHT);
        }

        else {
            angle += 180;
            robot_rotate_to(angle);
            angle = calibrate_angle_against_wall();
        }
    }
}
