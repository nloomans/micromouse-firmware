#pragma once
#include "../control/forward.h"
#include "../maze.h"

struct ir_distances robot_get_ir();
double robot_get_gyro();
void robot_rotate_to(double angle);
struct forward_result robot_forward(double angle, enum forward_stop_cond stop_cond);
void robot_step_back();

void robot_save_maze(const struct maze* maze);
void robot_load_maze(struct maze* maze);
