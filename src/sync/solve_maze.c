#include "solve_maze.h"
#include "../drivers/pins.h"
#include "../lib/macro.h"
#include "../log.h"
#include "../maze.h"
#include "../run.h"
#include "hardware/gpio.h"
#include "hardware/timer.h"
#include "pico/platform.h"
#include "pico/time.h"
#include "pico/types.h"
#include "robot.h"
#include <math.h>
#include <stdint.h>

#define TAG "solver"

struct score_table {
    uint8_t square[MAZE_SQUARES][MAZE_SQUARES];
};

static uint8_t score_table_get(const struct score_table* score_table, struct vector pos)
{
    return score_table->square[pos.y / 2][pos.x / 2];
}

static void score_table_set(struct score_table* score_table, struct vector pos, uint8_t value)
{
    score_table->square[pos.y / 2][pos.x / 2] = value;
}

static void score_table_init(struct score_table* score_table)
{
    for (int y = 0; y < MAZE_SQUARES; y++) {
        for (int x = 0; x < MAZE_SQUARES; x++) {
            score_table->square[y][x] = UINT8_MAX;
        }
    }
}

static void calculate_scores_r(struct score_table* score_table,
    const struct maze* maze, struct vector pos, enum car_dir start_car_dir, int score)
{
    // An equally good or a better path to this square already exist.
    if (score_table_get(score_table, pos) <= score) {
        return;
    }

    score_table_set(score_table, pos, score);

    for (enum car_dir car_dir = 0; car_dir < CAR_DIR_MAX; car_dir++) {
        if (maze_get_wall(maze, pos, car_dir) != MAZE_FIELD_WALL) {
            int new_score = score + 1;
            if (start_car_dir != car_dir) {
                new_score++;
            }
            calculate_scores_r(score_table, maze,
                maze_square_get_neighbour_pos(pos, car_dir), car_dir, new_score);
        }
    }
}

static void calculate_scores(struct score_table* score_table, const struct maze* maze, bool return_to_start)
{
    score_table_init(score_table);

    if (return_to_start) {
        calculate_scores_r(score_table, maze, maze->starting_square, CAR_DIR_NORTH, 0);
    } else {
        for (enum car_dir car_dir = 0; car_dir < CAR_DIR_MAX; car_dir++) {
            struct vector goals[] = CONFIG_MAZE_GOALS;

            for (size_t i = 0; i < ARRAY_SIZE(goals); i++) {
                calculate_scores_r(score_table, maze, goals[i], car_dir, 0);
            }
        }
    }
}

static int choose_rotation(const struct score_table* score_table, const struct maze* maze, struct vector pos, enum car_dir car_dir)
{
    int best_rotation = 0;
    uint8_t best_score = UINT8_MAX;

    for (int rotation = -1; rotation < 3; rotation++) {
        enum car_dir possible_car_dir = car_dir_rotate(car_dir, rotation);

        if (maze_get_wall(maze, pos, possible_car_dir) != MAZE_FIELD_EMPTY)
            continue;

        struct vector possible_pos = maze_square_get_neighbour_pos(pos, possible_car_dir);
        if (score_table_get(score_table, possible_pos) < best_score) {
            best_score = score_table_get(score_table, possible_pos);
            best_rotation = rotation;
        }
    }

    return best_rotation;
}

static double calibrate_angle_against_wall()
{
    robot_step_back();
    sleep_ms(200);
    return robot_get_gyro();
}

struct state {
    struct run run;
    double angle;
};

static int drive_forward(struct state* state)
{
    enum forward_stop_cond stop_cond
        = FORWARD_STOP_COND_GAP_LEFT
        | FORWARD_STOP_COND_WALL_FRONT
        | FORWARD_STOP_COND_GAP_RIGHT;

    struct forward_result result = robot_forward(state->angle, stop_cond);

    log_debugf(TAG, "drove %d units forward", result.units_driven);

    // Add filler events for all the squares we have driven, except for the
    // last. The last square will be handled by the main loop.
    for (int i = 0; i < result.units_driven - 1; i++) {
        struct run_event event_forward = {
            .type = RUN_EVENT_FORWARD,
            .data = {
                .forward = {
                    .squares = 1,
                },
            },
        };

        if (run_submit_event(&state->run, &event_forward) < 0) {
            return -1;
        }

        struct run_event event_capture = {
            .type = RUN_EVENT_CAPTURE,
            .data = {
                .capture = {
                    .wall_present = {
                        [DIR_LEFT] = true,
                        [DIR_FORWARD] = false,
                        [DIR_RIGHT] = true,
                    },
                },
            },
        };

        if (run_submit_event(&state->run, &event_capture) < 0) {
            return -1;
        }
    };

    struct run_event event_forward = {
        .type = RUN_EVENT_FORWARD,
        .data = {
            .forward = {
                .squares = 1,
            },
        },
    };

    if (run_submit_event(&state->run, &event_forward) < 0) {
        return -1;
    }

    return 0;
}

static int rotate_with(struct state* state, int rotation)
{
    state->angle += rotation * 90;
    robot_rotate_to(state->angle);

    struct run_event event = {
        .type = RUN_EVENT_ROTATE,
        .data = {
            .rotate = {
                .rotation = rotation,
            },
        },
    };

    if (run_submit_event(&state->run, &event) < 0) {
        return -1;
    }

    return 0;
}

static void handle_invalid_state(void)
{
    // TODO: activate the buzzer and switch to wall hugger mode
    log_errorf(TAG, "Invalid state!");

    gpio_init(PIN_BUZZER);
    gpio_set_dir(PIN_BUZZER, GPIO_OUT);

    for (int i = 0; i < 1000; i++) {
        gpio_put(PIN_BUZZER, 1);
        sleep_us(125);

        gpio_put(PIN_BUZZER, 0);
        sleep_us(125);
    }

    gpio_deinit(PIN_BUZZER);

    while (true) {
        tight_loop_contents();
    }
}

static double get_gyro_reset_interval_ms(void)
{
    double before = robot_get_gyro();
    sleep_ms(1000);
    double after = robot_get_gyro();
    log_debugf(TAG, "before: %.2f, after: %.2f", before, after);

    double drift_dps = fabs(after - before);
    if (drift_dps == 0) {
        return 100000;
    }

    double max_drift = 2;
    uint64_t timeout = (max_drift / drift_dps) * 1000;

    return timeout;
}

void solve_maze(void)
{
    bool return_to_start = false;

    struct maze maze;

    robot_load_maze(&maze);

    uint64_t gyro_reset_interval_ms = get_gyro_reset_interval_ms();

    struct state state = { 0 };
    run_init(&state.run, &maze);
    state.angle = calibrate_angle_against_wall();

    absolute_time_t gyro_reset_timeout = make_timeout_time_ms(gyro_reset_interval_ms);

    // The rules say that the starting square must have walls on the left and
    // right side. The only valid move is therefore to move forward.
    if (drive_forward(&state) < 0) {
        handle_invalid_state();
    }

    maze_print(&maze);

    while (true) {
        struct ir_distances distances = robot_get_ir();

        struct run_event_capture capture = {
            .wall_present = {
                [DIR_LEFT] = distances.sensor_mm[IR_LEFT] < 100,
                [DIR_FORWARD] = distances.sensor_mm[IR_FRONT] < 100,
                [DIR_RIGHT] = distances.sensor_mm[IR_RIGHT] < 100,
            },
        };

        struct run_event event = {
            .type = RUN_EVENT_CAPTURE,
            .data = {
                .capture = capture,
            },
        };

        if (run_submit_event(&state.run, &event) < 0) {
            handle_invalid_state();
        }

        maze_print_with_robot(&maze, state.run.coord, state.run.car_dir);

        struct score_table score_table;
        calculate_scores(&score_table, &maze, return_to_start);

        if (score_table_get(&score_table, state.run.coord) == 0) {
            log_infof(TAG, "finished in %d steps!", state.run.events_len);
            robot_save_maze(&maze);

            if (return_to_start) {
                return;
            }

            return_to_start = true;
            calculate_scores(&score_table, &maze, return_to_start);
        }

        int rotation = choose_rotation(&score_table, &maze, state.run.coord, state.run.car_dir);

        if (rotation != 0) {
            if (rotate_with(&state, rotation) < 0) {
                handle_invalid_state();
            }

            if (time_reached(gyro_reset_timeout)
                && maze_get_wall(&maze, state.run.coord, car_dir_rotate(state.run.car_dir, -2)) == MAZE_FIELD_WALL) {

                state.angle = calibrate_angle_against_wall();
                gyro_reset_timeout = make_timeout_time_ms(gyro_reset_interval_ms);
            }
        }

        if (drive_forward(&state) < 0) {
            handle_invalid_state();
        }
    }
}
