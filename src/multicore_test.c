#include "lib/ipc.h"
#include "shell_compat.h"
#include <pico/multicore.h>
#include <stdio.h>

static int ping(int message)
{
    const struct ipc_request request = {
        .op = IPC_OP_PING,
        .data = {
            .ping = message,
        },
    };

    union ipc_response response;
    ipc_send(&response, &request);
    return response.ping;
}

static void core1_main()
{
    int message = 0;
    while (true) {
        int message_echo = ping(message);
        printf("[core1] ping: %d, pong: %d\n", message, message_echo);
        sleep_ms(100);
        message++;
    }
}

static void cmd_test(shell_compat_process_t* process, int argc, char** argv)
{
    static const struct ipc_request* request;

    if (shell_compat_iteration(process) == 0) {
        request = NULL;
        shell_compat_set_cleanup_callback(process, multicore_reset_core1);
        multicore_launch_core1(core1_main);
    }

    if (request == NULL) {
        request = ipc_recv();
    }

    if (request != NULL) {
        switch (request->op) {
        case IPC_OP_PING: {
            if (shell_compat_iteration(process) % 50 != 0) {
                return;
            }

            union ipc_response response = {
                .ping = request->data.ping,
            };
            ipc_reply(&request, &response);

            break;
        }
        default:
            shell_compat_exit(process);
        }

        return;
    }
}

void multicore_test_init()
{
    shell_compat_register("multicore-test", "Test multicore IPC", cmd_test);
}
