#include "program.h"
#include "FreeRTOS.h"
#include <semphr.h>
#include <string.h>

struct program_list {
    SemaphoreHandle_t mutex;
    const struct program* head;
};

static struct program_list program_list;

void program_register_struct(struct program* program)
{
    xSemaphoreTake(program_list.mutex, portMAX_DELAY);
    program->next = program_list.head;
    program_list.head = program;
    xSemaphoreGive(program_list.mutex);
}

const struct program* program_find(const char* name)
{
    xSemaphoreTake(program_list.mutex, portMAX_DELAY);
    const struct program* head = program_list.head;
    xSemaphoreGive(program_list.mutex);

    for (const struct program* program = head; program != NULL; program = program->next) {
        if (strcmp(program->name, name) == 0) {
            return program;
        }
    }

    return NULL;
}

void program_init(void)
{
    static StaticSemaphore_t program_list_mutex_store;
    program_list.mutex = xSemaphoreCreateMutexStatic(&program_list_mutex_store);
}
