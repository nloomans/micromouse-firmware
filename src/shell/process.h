#pragma once

typedef struct process* process_t;

/**
 * process_wait waits until the user presses ^C. Program should run any cleanup
 * commands after this function returns.
 */
void process_wait(process_t process);

/**
 * program_should_exit returns  true if the user has requested the process to
 * stop using ^C.
 */
bool process_should_exit(process_t process);

void process_request_exit(process_t process);

struct program;

process_t process_spawn(struct program* program, int argc, char** argv,
    void (*callback)(void*), void* callback_param);
