#pragma once
#include "process.h"

/**
 * The implementation of a program. It is launched in a dedecated task and
 * expected to return upon completion. Long-running programs must make sure
 * they return when ^C is pressed using [TODO].
 */
typedef void (*program_function_t)(process_t process, int argc, char** argv);

struct program {
    /**
     * Command name that should entered in the shell to executed the program.
     */
    const char* name;
    /**
     * Description listed by the "help" command.
     */
    const char* description;
    /**
     * Function that implements the program.
     */
    program_function_t function;
    /**
     * Arbitrary data that is passed to the function when the program is
     * executed.
     *
     * Use process_data() to access.
     */
    void* data;
    /**
     * Link to the next program. Automatically filled in when a program is
     * registered.
     */
    const struct program* next;
};

/**
 * program_register_struct register a new program.
 *
 * The program struct must be statically allocated and may not be modified
 * after registration.
 */
void program_register_struct(struct program* program);

const struct program* program_find(const char* name);

void program_init();
