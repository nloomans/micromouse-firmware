#include "shell_compat.h"
#include <semphr.h>
#include <task.h>

struct shell_compat_process {
    shell_compat_function_t func;
    int argc;
    char** argv;
    bool active;
    int iteration;
    shell_compat_callback_t cleanup_callback;
    shell_process_t* real_process;
    TaskHandle_t notify_on_exit;
};

struct shared_state {
    SemaphoreHandle_t mutex;
    struct shell_compat_process* process;
};

static struct shared_state shared_state;

int shell_compat_iteration(shell_compat_process_t* process)
{
    return process->iteration;
}

void shell_compat_set_cleanup_callback(shell_compat_process_t* process,
    shell_compat_callback_t callback)
{
    process->cleanup_callback = callback;
}

void shell_compat_exit(shell_compat_process_t* process)
{
    process->active = false;
}

void shell_compat_tick(void)
{
    xSemaphoreTake(shared_state.mutex, portMAX_DELAY);

    if (shared_state.process) {
        struct shell_compat_process* process = shared_state.process;

        process->func(process, process->argc, process->argv);

        if (shell_should_exit(process->real_process) || !process->active) {
            if (process->cleanup_callback) {
                process->cleanup_callback();
            }

            xTaskNotifyGive(process->notify_on_exit);
            shared_state.process = NULL;
        } else {
            process->iteration++;
        }
    }

    xSemaphoreGive(shared_state.mutex);
}

void shell_compat_wrapper(shell_process_t* process, int argc, char** argv)
{
    struct shell_compat_process compat_process = {
        .func = shell_data(process),
        .argc = argc,
        .argv = argv,
        .active = true,
        .real_process = process,
        .notify_on_exit = xTaskGetCurrentTaskHandle(),
    };

    xSemaphoreTake(shared_state.mutex, portMAX_DELAY);
    shared_state.process = &compat_process;
    xSemaphoreGive(shared_state.mutex);

    ulTaskNotifyTake(false, portMAX_DELAY);
}

void shell_compat_init(void)
{
    static StaticSemaphore_t shared_state_mutex_store;
    shared_state.mutex = xSemaphoreCreateMutexStatic(&shared_state_mutex_store);
}
