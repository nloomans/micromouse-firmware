#include "run.h"
#include "log.h"
#include "maze.h"
#include "pico/assert.h"
#include "units.h"
#include <assert.h>
#include <stdbool.h>
#include <string.h>

#define TAG "run"

void run_init(struct run* run, struct maze* maze)
{
    memset(run, 0, sizeof(*run));
    run->maze = maze;
}

static void enqueue_event(struct run* run, struct run_event* event)
{
    assert(run->event_queue_len < sizeof(run->event_queue) / sizeof(*run->event_queue));
    run->event_queue[run->event_queue_len++] = *event;
}

static int drain_event_queue(struct run* run)
{
    for (size_t i = 0; i < run->event_queue_len; i++) {
        log_infof(TAG, "submitting queued event %zu/%zu",
            i + 1, run->event_queue_len);

        int err = run_submit_event(run, &run->event_queue[i]);

        // An event can only fail to submit if it contained information that
        // contradicts the infomation we already have. This is the case if the
        // event said that there wasn't a wall on either the left or the right
        // side, while we know there is (or vice-verse). However, if an event
        // detects that, it would have never ended up in this queue in the
        // first place, since from that point on we know our starting position.
        //
        // If we do end up here there is something very wrong with our logic,
        // and there wouldn't be any sane way to continue (returning the error
        // would leave the event queue in an invalid state). Use a hard_assert
        // so that this assert won't get optimized away.
        hard_assert(err == 0);
    }

    // There shouldn't ever be queued events after this, but reset the state
    // anyway to be nice.
    memset(&run->event_queue, 0, sizeof(run->event_queue));
    run->event_queue_len = 0;

    return 0;
}

int run_submit_event(struct run* run, struct run_event* event)
{
    assert(run->events_len < RUN_MAX_EVENTS);

    switch (event->type) {
    case RUN_EVENT_CAPTURE:
        log_debugf(TAG, "RUN_EVENT_CAPTURE { %d, %d, %d }",
            event->data.capture.wall_present[DIR_LEFT],
            event->data.capture.wall_present[DIR_FORWARD],
            event->data.capture.wall_present[DIR_RIGHT]);
        break;
    case RUN_EVENT_ROTATE:
        log_debugf(TAG, "RUN_EVENT_ROTATE { %d }",
            event->data.rotate.rotation);
        break;
    case RUN_EVENT_FORWARD:
        log_debugf(TAG, "RUN_EVENT_FORWARD { %d }",
            event->data.forward.squares);
        break;
    }

    // We don't have enough information yet to know where we start.
    if (run->coord.y == 0 && run->coord.x == 0) {
        log_debugf(TAG, "1");
        enqueue_event(run, event);

        if (event->type == RUN_EVENT_CAPTURE) {
            log_debugf(TAG, "2");
            struct run_event_capture capture = event->data.capture;

            if (!capture.wall_present[DIR_LEFT] && !capture.wall_present[DIR_RIGHT]) {
                log_errorf(TAG, "maze cannot have both sides open at this point.");
                return -1;
            }

            if (!capture.wall_present[DIR_LEFT]) {
                log_debugf(TAG, "3");
                if (!maze_is_starting_square_known(run->maze)) {
                    maze_set_starting_square(run->maze,
                        MAZE_STARTING_SQUARE_BOTTOM_RIGHT);
                }
                run->coord = MAZE_STARTING_SQUARE_BOTTOM_RIGHT;
                drain_event_queue(run);
            } else if (!capture.wall_present[DIR_RIGHT]) {
                log_debugf(TAG, "4");
                if (!maze_is_starting_square_known(run->maze)) {
                    maze_set_starting_square(run->maze,
                        MAZE_STARTING_SQUARE_BOTTOM_LEFT);
                }
                run->coord = MAZE_STARTING_SQUARE_BOTTOM_LEFT;
                drain_event_queue(run);
            }
        }

        log_debugf(TAG, "5");
        return 0;
    }

    run->events[run->events_len++] = *event;

    switch (event->type) {
    case RUN_EVENT_CAPTURE: {
        struct run_event_capture capture = event->data.capture;

        for (size_t dir = DIR_LEFT; dir <= DIR_RIGHT; dir++) {
            enum car_dir car_dir = car_dir_rotate(
                run->car_dir, DIR_FORWARD - dir);

            enum maze_field old_value = maze_get_wall(
                run->maze, run->coord, car_dir);

            enum maze_field new_value = capture.wall_present[dir]
                ? MAZE_FIELD_WALL
                : MAZE_FIELD_EMPTY;

            if (old_value != MAZE_FIELD_UNKNOWN && old_value != new_value) {
                log_errorf(TAG, "new value conflicts with old value.");
                return -1;
            }

            maze_set_wall(run->maze, run->coord, car_dir, new_value);
        }

        maze_set_field(run->maze, run->coord, MAZE_FIELD_EMPTY);

        break;
    }
    case RUN_EVENT_ROTATE: {
        struct run_event_rotate rotate = event->data.rotate;

        run->car_dir = car_dir_rotate(run->car_dir, rotate.rotation);

        break;
    }

    case RUN_EVENT_FORWARD: {
        struct run_event_forward forward = event->data.forward;

        for (int i = 0; i < forward.squares; i++) {
            run->coord = maze_square_get_neighbour_pos(run->coord, run->car_dir);
        }

        break;
    }
    }

    return 0;
}
