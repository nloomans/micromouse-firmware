#include "legacy_main.h"
#include "lib/macro.h"
#include "shell.h"
#include "shell_compat.h"
#include <hardware/watchdog.h>
#include <pico/binary_info.h>
#include <pico/stdio_usb.h>
#include <pico/stdlib.h>
#include <stdio.h>
#include <task.h>

void vApplicationIdleHook(void)
{
    watchdog_update();
}

_Noreturn void safe_mode()
{
    while (!stdio_usb_connected()) {
        // Wait until someone opened /dev/ttyACM0 before printing the message,
        // otherwise we are just screaming into the void.
    }

    puts("WATCHDOG TRIGGERED - SAFE MODE");
    puts("Reboot device to return to regular mode.");

    macro_noreturn();
}

int main(void)
{
    bi_decl(bi_program_description("Micromouse Firmware"));
    bi_decl(bi_program_version_string("v0.0.1"));

    stdio_init_all();

    if (watchdog_enable_caused_reboot()) {
        // Don't continue normal setup, that may trigger the watchdog that just
        // killed us again.
        safe_mode();
    }

    // Require the watchdog to be updated every 1 s. Chosen arbitrarily.
    watchdog_enable(1000, true);

    shell_init();
    shell_compat_init();
    legacy_main_init();

    vTaskStartScheduler();
}
