#pragma once
#include <stdbool.h>
#include <stdint.h>

enum car_dir {
    CAR_DIR_NORTH,
    CAR_DIR_EAST,
    CAR_DIR_SOUTH,
    CAR_DIR_WEST,
};
#define CAR_DIR_MAX 4

enum dir {
    DIR_LEFT,
    DIR_FORWARD,
    DIR_RIGHT,
};
#define DIR_MAX 3

struct vector {
    int8_t y;
    int8_t x;
};

enum car_dir car_dir_rotate(enum car_dir car_dir, int rotation);
struct vector car_dir_to_vector(enum car_dir car_dir);
struct vector vector_add(struct vector a, struct vector b);
bool vector_equal(struct vector a, struct vector b);

#define square_to_vector(x, y) \
    ((struct vector) { (x) * 2 + 1, (y) * 2 + 1 })
