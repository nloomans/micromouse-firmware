#include "shell.h"
#include "drivers/button.h"
#include "lib/macro.h"
#include "task_config.h"
#include <ctype.h>
#include <event_groups.h>
#include <pico/stdio.h>
#include <queue.h>
#include <semphr.h>
#include <stdio.h>
#include <string.h>

#define SHELL_BUFFER_SIZE 64
#define SHELL_MAX_ARGS 16

#define EVENT_CHARS_AVAILABLE_BIT (1 << 0)
#define EVENT_PROCESS_EXITED_BIT (1 << 1)

struct shell_process {
    const struct shell_command* command;
    int argc;
    char** argv;

    // Written to when the shell process should exit (for example, because the
    // user processed CTRL+C). Queue of 1 item with size of 0 bytes.
    QueueHandle_t should_exit;

    // Process must write EVENT_PROCESS_EXITED_BIT to event group on exit
    EventGroupHandle_t shell_events;
};

static SemaphoreHandle_t command_head_mutex;
static const struct shell_command* command_head = NULL;

bool shell_should_exit(shell_process_t* process)
{
    return xQueuePeek(process->should_exit, NULL, 0);
}

void shell_wait(shell_process_t* process)
{
    xQueuePeek(process->should_exit, NULL, portMAX_DELAY);
}

void* shell_data(shell_process_t* process)
{
    return process->command->data;
}

void shell_register_struct(struct shell_command* command)
{
    xSemaphoreTake(command_head_mutex, portMAX_DELAY);
    command->next = command_head;
    command_head = command;
    xSemaphoreGive(command_head_mutex);
}

static const struct shell_command* find_command(const char* command_name)
{
    xSemaphoreTake(command_head_mutex, portMAX_DELAY);
    const struct shell_command* head = command_head;
    xSemaphoreGive(command_head_mutex);

    const struct shell_command* command;
    for (command = head; command != NULL; command = command->next) {
        if (strcmp(command->name, command_name) == 0) {
            return command;
        }
    }

    return NULL;
}

// TODO: What to do with shortcuts? Do they even make sense in a world of
// tasks?
static const char* shortcuts[BUTTON_COUNT] = { 0 };

void shell_register_shortcut(enum button button, const char* command)
{
    int button_index = button_to_index(button);
    if (button_index == -1) {
        printf("[shell] dropping shortcut '%s': invalid button %#x\n", command, button);
        return;
    }
    if (shortcuts[button_index] != NULL) {
        printf("[shell] dropping shortcut '%s': button %x is already used for shortcut '%s'\n",
            command, button_index, shortcuts[button_index]);
        return;
    }
    shortcuts[button_to_index(button)] = command;
}

static void shell_process_task(void* data)
{
    struct shell_process* process = data;

    process->command->func(process, process->argc, process->argv);

    xEventGroupSetBits(process->shell_events, EVENT_PROCESS_EXITED_BIT);
    vTaskSuspend(NULL);
}

static void run_shell_command(EventGroupHandle_t shell_events, const struct shell_command* command, int argc, char** argv)
{
    StaticQueue_t process_should_exit_store;

    struct shell_process process = {
        .command = command,
        .argc = argc,
        .argv = argv,
        .shell_events = shell_events,
        .should_exit = xQueueCreateStatic(1, 0, NULL, &process_should_exit_store),
    };

    static StackType_t program_stack[configMINIMAL_STACK_SIZE];
    static StaticTask_t program_task_store;
    TaskHandle_t process_handle = xTaskCreateStatic(&shell_process_task, argv[0],
        ARRAY_SIZE(program_stack),
        &process,
        TASK_PRIO_SHELL_PROCESS,
        program_stack,
        &program_task_store);

    while (true) {
        EventBits_t events = xEventGroupWaitBits(shell_events,
            EVENT_CHARS_AVAILABLE_BIT | EVENT_PROCESS_EXITED_BIT,
            true, false, portMAX_DELAY);

        if (events & EVENT_PROCESS_EXITED_BIT) {
            break;
        }

        if (events & EVENT_CHARS_AVAILABLE_BIT) {
            if (getchar_timeout_us(0) == '\x03' /* CTRL+C */) {
                // Kindly ask the process to exit.
                xQueueOverwrite(process.should_exit, NULL);
            }
        }
    }

    // We want to suspend the task before deleting it, since if the task if
    // running while we are trying to delete it, vTaskDelete will instead
    // instruct the IDLE task to delete the task. This is an issue since
    // our program wants to be able to reuse the program_stack and
    // program_static_task variables for the next program to be executed.
    vTaskSuspend(process_handle);
    vTaskDelete(process_handle);

    vQueueDelete(process.should_exit);
}

static void print_prompt()
{
    printf("\033[0;94mfemtofant\033[0;34m>\033[0m ");
}

static void chars_available_callback(void* data)
{
    EventGroupHandle_t shell_events = data;

    BaseType_t higher_priority_task_woken = false;
    xEventGroupSetBitsFromISR(shell_events, EVENT_CHARS_AVAILABLE_BIT,
        &higher_priority_task_woken);
    portYIELD_FROM_ISR(higher_priority_task_woken);
}

static void readline(char* buffer, size_t buffer_size, EventGroupHandle_t shell_events)
{
    memset(buffer, 0, buffer_size);

    print_prompt();

    while (true) {
        // TODO: check for button shortcuts
        xEventGroupWaitBits(shell_events, EVENT_CHARS_AVAILABLE_BIT,
            true, false, portMAX_DELAY);

        int c;
        while ((c = getchar_timeout_us(0)) > 0) {
            if (!iscntrl(c)) {
                // Add a new character to the line
                size_t len = strlen(buffer);
                if (len + 1 < buffer_size) {
                    printf("%c", c);
                    buffer[len] = c;
                }
            } else if (c == '\x7f' /* DEL */) {
                // Remove the last character from the line
                size_t len = strlen(buffer);
                if (len > 0) {
                    printf("\b \b");
                    buffer[len - 1] = 0;
                }
            } else if (c == '\r' || c == '\n') {
                printf("\n");
                return;
            }
        }
    }

    return;
}

static int buffer_to_args(char** argv, int argc_max, char* buffer)
{
    int argc = 0;

    char* strtok_arg = buffer;
    while (argc < argc_max) {
        char* word = strtok(strtok_arg, " ");
        if (word == NULL) {
            break;
        }

        argv[argc++] = word;

        strtok_arg = NULL;
    }

    return argc;
}

static void shell_task(void* data)
{
    EventGroupHandle_t shell_events = data;

    while (true) {
        char buffer[SHELL_BUFFER_SIZE] = { 0 };
        readline(buffer, sizeof(buffer), shell_events);

        char* argv[SHELL_MAX_ARGS] = { NULL };
        int argc = buffer_to_args(argv, ARRAY_SIZE(argv), buffer);

        if (argc == 0)
            continue;

        const struct shell_command* command = find_command(argv[0]);
        if (command == NULL) {
            printf("Unkown command\n");
            continue;
        }

        run_shell_command(shell_events, command, argc, argv);
    }
}

static void cmd_help(shell_process_t* process, int argc, char** argv)
{
    xSemaphoreTake(command_head_mutex, portMAX_DELAY);
    const struct shell_command* head = command_head;
    xSemaphoreGive(command_head_mutex);

    const struct shell_command* command;
    for (command = head; command != NULL; command = command->next) {
        printf("%s: %s\n", command->name, command->description);
    }
}

void shell_init(void)
{
    static StaticSemaphore_t command_head_mutex_store;
    command_head_mutex = xSemaphoreCreateMutexStatic(&command_head_mutex_store);

    static StaticEventGroup_t shell_events_store;
    EventGroupHandle_t shell_events = xEventGroupCreateStatic(&shell_events_store);

    stdio_set_chars_available_callback(chars_available_callback, shell_events);

    static StackType_t shell_stack[configMINIMAL_STACK_SIZE + 2048];
    static StaticTask_t shell_task_store;

    xTaskCreateStatic(&shell_task, "Shell",
        ARRAY_SIZE(shell_stack),
        shell_events,
        TASK_PRIO_SHELL,
        shell_stack,
        &shell_task_store);

    shell_register("help", "Print this help text", cmd_help);
}
