#pragma once
#include "shell.h"

typedef struct shell_compat_process shell_compat_process_t;

typedef void (*shell_compat_function_t)(shell_compat_process_t* process, int argc, char** argv);

typedef void (*shell_compat_callback_t)(void);

/**
 * shell_compat_iteration returns how many times the given process has been
 * called, starting at 0.
 */
int shell_compat_iteration(shell_compat_process_t* process);

/**
 * shell_compat_set_cleanup_callback registers a callback function that will be
 * called when the process exists.
 */
void shell_compat_set_cleanup_callback(shell_compat_process_t* process, shell_compat_callback_t callback);

/**
 * shell_compat_exit can be called by a process to signal that it shouldn't be
 * called again next tick.
 */
void shell_compat_exit(shell_compat_process_t* process);

/**
 * shell_compat_wrapper is an internal function that glues old-style shell
 * commands with the new task api.
 */
void shell_compat_wrapper(shell_process_t* process, int argc, char** argv);

void shell_compat_tick(void);
void shell_compat_init(void);

/**
 * shell_compat_register registers a new shell command. It should called in the
 * init function of a module that wishes to register a shell command.
 *
 * cmd_name: What the user needs to type in the shell to run the command
 * cmd_description: Shown together with your command when running "help"
 * cmd_func: Function that will be repeatetly during execution of the command.
 *           Must be of type shell_compat_function_t.
 */
#define shell_compat_register(cmd_name, cmd_description, cmd_func) \
    static struct shell_command __shell_command_##cmd_func = {     \
        .name = cmd_name,                                          \
        .description = cmd_description,                            \
        .func = shell_compat_wrapper,                              \
        .data = cmd_func,                                          \
    };                                                             \
                                                                   \
    shell_register_struct(&__shell_command_##cmd_func)
