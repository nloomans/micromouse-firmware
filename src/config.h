#pragma once

#include "units.h"

/**
 * The amount of units that the maze is wide.
 */
#define CONFIG_MAZE_SIZE 16

/**
 * The goal squares.
 */
#define CONFIG_MAZE_GOALS { \
    square_to_vector(8, 8), \
    square_to_vector(8, 9), \
    square_to_vector(9, 8), \
    square_to_vector(9, 9), \
}
