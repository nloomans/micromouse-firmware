cmake_minimum_required(VERSION 3.13)

option(STRICT_WERROR "Enable all werror flags")

include(pico-sdk/pico_sdk_init.cmake)

project(micromouse_project C CXX ASM)
set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
pico_sdk_init()

add_subdirectory(freertos-kernel/portable/ThirdParty/GCC/RP2040 FREERTOS_KERNEL)

set(sources
	src/control/control.c
	src/control/forward.c
	src/control/ir_pd.c
	src/control/rotate.c
	src/drivers/button.c
	src/drivers/gyro.c
	src/drivers/ir.c
	src/drivers/mpu6500.c
	src/drivers/shift.c
	src/drivers/motor.c
	src/io/i2c_dev.c
	src/lib/ipc.c
	src/sync/follow_wall.c
	src/sync/solve_maze.c
	src/sync/robot.c
	src/test/solve_maze.c
	src/interrupt.c
	src/legacy_main.c
	src/log.c
	src/main.c
	src/maze.c
	src/multicore_test.c
	src/run.c
	src/shell.c
	src/shell_compat.c
	src/units.c
)
add_executable(micromouse_firmware ${sources})

target_include_directories(micromouse_firmware PRIVATE
	# Needed for FreeRTOS to find FreeRTOSConfig.h
	${CMAKE_CURRENT_LIST_DIR}/include
)

pico_enable_stdio_usb(micromouse_firmware 1)
pico_enable_stdio_uart(micromouse_firmware 0)
pico_add_extra_outputs(micromouse_firmware)

target_link_libraries(micromouse_firmware
	pico_rand
	pico_stdlib
	pico_multicore
	hardware_pwm
	hardware_adc
	hardware_dma
	hardware_i2c
	FreeRTOS-Kernel-Static
)

# The test board we are using for now won't boot without this because of a
# hardware issue
target_compile_definitions(micromouse_firmware PRIVATE PICO_XOSC_STARTUP_DELAY_MULTIPLIER=64)

# Project-specific compile options
set(compile_options
	# Good baseline of warnings
	-Wall
	-Wextra

	# We often don't use all function arguments, especially when implementing
	# shell_function_t. This is common enough that it is better to just disable
	# this warning.
	-Wno-unused-parameter

	# Turn all warnings into errors. It's good to solve warnings right away,
	# since they may be the cause of whatever issue you are debugging.
	-Werror

	# This makes sure that we also get colors when using ninja.
	-fdiagnostics-color=always

	# Always include FreeRTOS.h, so we don't need to care about import order.
	$<$<COMPILE_LANGUAGE:C>:-include FreeRTOS.h>
)

if (NOT STRICT_WERROR)
	# These warnigns should stay as warnings, however, since they are common
	# during development (like when commenting out a code block). CI will still
	# error on them.
	list(APPEND compile_options
		-Wno-error=unused-but-set-parameter
		-Wno-error=unused-but-set-variable
		-Wno-error=unused-const-variable
		-Wno-error=unused-function
		-Wno-error=unused-label
		-Wno-error=unused-local-typedefs
		-Wno-error=unused-value
		-Wno-error=unused-variable
		-Wno-error=unused-variable
	)
endif()

set_source_files_properties(${sources} PROPERTIES COMPILE_OPTIONS "${compile_options}")
